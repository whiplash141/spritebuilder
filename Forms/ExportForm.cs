﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Whiplash.Utils;

namespace SpriteBuilder
{
    public partial class ExportForm : Form
    {
        List<SpriteInfo> _spriteInfoList;

        StringBuilder _stringBuilder = new StringBuilder(1024),
                      _spriteListStringBuilder = new StringBuilder(1024);

        Color _canvasColor;

        static readonly Regex _functionNameRegex = new Regex(Constants.AllowedFunctionNamePattern);

        public ExportForm(List<SpriteInfo> spriteInfo, Color canvasColor)
        {
            InitializeComponent();

            _spriteInfoList = spriteInfo;
            _canvasColor = canvasColor;

            checkedListBox_Rotation.BeginUpdate();
            foreach (SpriteInfo info in _spriteInfoList)
            {
                checkedListBox_Rotation.Items.Add(info.Name);
            }
            checkedListBox_Rotation.EndUpdate();
        }

        private string CodeExport()
        {
            _stringBuilder.Clear();
            
            bool useColorScale = checkBoxCsColorScale.Checked;
            bool useConstructor = checkBoxCsConstructor.Checked;
            string codeTemplate = useConstructor ? Constants.TextureSpriteCodeConstructorTemplate : Constants.SpriteCodeTemplate;
            string textCodeTemplate = useConstructor ? Constants.TextSpriteCodeConstructorTemplate : Constants.TextSpriteCodeTemplate;
            string colorScale = useColorScale ? ", float colorScale = 1f" : "";
            string colorFormat = useColorScale ? "{0}f*colorScale" : "{0}";
            string colorFormatAlpha = useColorScale ? "{0}f" : "{0}";

            bool rotationNeedsTrig = false;
            bool usedRotation = false;
            for (int i = _spriteInfoList.Count - 1; i >= 0; --i)
            {
                SpriteInfo spriteInfo = _spriteInfoList[i];
                float red = useColorScale ? spriteInfo.Color.R / 255f : spriteInfo.Color.R;
                float green = useColorScale ? spriteInfo.Color.G / 255f : spriteInfo.Color.G;
                float blue = useColorScale ? spriteInfo.Color.B / 255f : spriteInfo.Color.B;
                float alpha = useColorScale ? spriteInfo.Color.A / 255f : spriteInfo.Color.A;
                string spriteString;

                string vectorTemplate;

                bool shouldRotate = checkedListBox_Rotation.GetItemChecked(i + 1);
                usedRotation |= shouldRotate;
                if (shouldRotate)
                {
                    if (spriteInfo.Position.X == 0 && spriteInfo.Position.Y == 0)
                    {
                        vectorTemplate = Constants.SpriteVectorFormat;
                    }
                    else if (spriteInfo.Position.X == 0)
                    {
                        vectorTemplate = Constants.SpriteVectorFormatWithRotationZeroX;
                        rotationNeedsTrig = true;
                    }
                    else if (spriteInfo.Position.Y == 0)
                    {
                        vectorTemplate = Constants.SpriteVectorFormatWithRotationZeroY;
                        rotationNeedsTrig = true;
                    }
                    else
                    {
                        vectorTemplate = Constants.SpriteVectorFormatWithRotation;
                        rotationNeedsTrig = true;
                    }
                }
                else
                {
                    vectorTemplate = Constants.SpriteVectorFormat;
                }

                if (spriteInfo.Type == SpriteType.TEXTURE)
                {
                    string rotationTemplate;
                    if (shouldRotate)
                    {
                        if (spriteInfo.Rotation == 0)
                        {
                            rotationTemplate = Constants.SpriteRotationFormatWithRotationZero;
                        }
                        else
                        {
                            rotationTemplate = Constants.SpriteRotationFormatWithRotation;
                        }
                    }
                    else
                    {
                        rotationTemplate = Constants.SpriteRotationFormat;
                    }

                    spriteString = string.Format(CultureInfo.InvariantCulture,
                                                        codeTemplate,
                                                        spriteInfo.SpriteType,
                                                        string.Format(
                                                            CultureInfo.InvariantCulture,
                                                            vectorTemplate,
                                                            spriteInfo.Position.X,
                                                            spriteInfo.Position.Y
                                                        ),
                                                        spriteInfo.Size.X,
                                                        spriteInfo.Size.Y,
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, red),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, green),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, blue),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormatAlpha, alpha),
                                                        string.Format(
                                                            CultureInfo.InvariantCulture,
                                                            rotationTemplate,
                                                            spriteInfo.Rotation / 180f * Math.PI),
                                                        spriteInfo.Name,
                                                        spriteInfo.Alignment
                                                    );
                }
                else
                {
                    spriteString = string.Format(CultureInfo.InvariantCulture,
                                                        textCodeTemplate,
                                                        spriteInfo.SpriteType.Replace(Environment.NewLine, @"\n"),
                                                        string.Format(
                                                            CultureInfo.InvariantCulture,
                                                            vectorTemplate,
                                                            spriteInfo.Position.X,
                                                            spriteInfo.Position.Y
                                                        ),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, red),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, green),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormat, blue),
                                                        string.Format(CultureInfo.InvariantCulture, colorFormatAlpha, alpha),
                                                        spriteInfo.Font,
                                                        string.Format(CultureInfo.InvariantCulture, "{0:0.####}f", spriteInfo.Rotation),
                                                        spriteInfo.Name,
                                                        spriteInfo.Alignment
                                                    );
                }
                _stringBuilder.AppendLine(spriteString);
            }

            string funcTemplate;
            if (usedRotation)
            {
                funcTemplate = rotationNeedsTrig? Constants.SpriteFunctionTemplateWithRotation: Constants.SpriteFunctionTemplateWithRotationNoTrig;
            }
            else
            {
                funcTemplate = Constants.SpriteFunctionTemplate;
            }
           
            string output = string.Format(CultureInfo.InvariantCulture,
                                          funcTemplate,
                                          _canvasColor.R,
                                          _canvasColor.G,
                                          _canvasColor.B,
                                          _canvasColor.A,
                                          textBoxExportFunctionName.Text,
                                          colorScale,
                                          _stringBuilder.ToString());
            Logger.Default.WriteLine("Sprites exported to C#");
            return output;
        }

        private string CustomDataExport()
        {
            _stringBuilder.Clear();
            _spriteListStringBuilder.Clear();

            for (int i = _spriteInfoList.Count - 1; i >= 0; --i)
            {
                SpriteInfo spriteInfo = _spriteInfoList[i];
                string spriteString;
                string prefix;
                if (spriteInfo.Type == SpriteType.TEXTURE)
                {
                    prefix = Constants.CustomDataTextureSpritePrefix;
                    spriteString = string.Format(CultureInfo.InvariantCulture,
                                                        Constants.CustomDataTextureSpriteTemplate,
                                                        spriteInfo.Name,
                                                        spriteInfo.SpriteType,
                                                        spriteInfo.Position.X,
                                                        spriteInfo.Position.Y,
                                                        spriteInfo.Size.X,
                                                        spriteInfo.Size.Y,
                                                        spriteInfo.Color.R,
                                                        spriteInfo.Color.G,
                                                        spriteInfo.Color.B,
                                                        spriteInfo.Color.A,
                                                        spriteInfo.Rotation / 180f * Math.PI,
                                                        spriteInfo.Alignment);
                }
                else
                {
                    prefix = Constants.CustomDataTextSpritePrefix;
                    string textContent;
                    if (spriteInfo.SpriteType.Contains(Environment.NewLine))
                    {
                        textContent = $"{Environment.NewLine}|{spriteInfo.SpriteType.Replace(Environment.NewLine, $"{Environment.NewLine}|")}";
                    }
                    else
                    {
                        textContent = spriteInfo.SpriteType;
                    }
                    spriteString = string.Format(CultureInfo.InvariantCulture,
                                                        Constants.TextCustomDataSpriteTemplate,
                                                        spriteInfo.Name,
                                                        textContent,
                                                        spriteInfo.Position.X,
                                                        spriteInfo.Position.Y,
                                                        spriteInfo.Color.R,
                                                        spriteInfo.Color.G,
                                                        spriteInfo.Color.B,
                                                        spriteInfo.Color.A,
                                                        spriteInfo.Font,
                                                        spriteInfo.Rotation,
                                                        spriteInfo.Alignment);
                }
                _stringBuilder.AppendLine(spriteString);
                _spriteListStringBuilder.Append($"\r\n|{prefix}:{spriteInfo.Name}");
            }

            int screenIdx = (int)numericUpDownScreenIndex.Value;
            string output = string.Format(CultureInfo.InvariantCulture,
                                                 Constants.CustomDataScreenTemplate,
                                                 screenIdx,
                                                 _canvasColor.R,
                                                 _canvasColor.G,
                                                 _canvasColor.B,
                                                 _canvasColor.A,
                                                 _spriteListStringBuilder,
                                                 _stringBuilder);
            Logger.Default.WriteLine("Sprites exported to custom data");
            return output;
        }

        private void buttonExportCustomData_Click(object sender, EventArgs e)
        {
            textBoxExport.Text = CustomDataExport();
        }

        private void textBoxExportFunctionName_TextChanged(object sender, EventArgs e)
        {
            if (_functionNameRegex.IsMatch(textBoxExportFunctionName.Text))
            {
                buttonExport.Enabled = true;
                textBoxExportFunctionName.BackColor = default(Color);
            }
            else
            {
                buttonExport.Enabled = false;
                textBoxExportFunctionName.BackColor = Color.Red;
            }
        }

        bool _suppressItemCheck = false;
        private void checkedListBox_Rotation_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (_suppressItemCheck)
            {
                return;
            }

            _suppressItemCheck = true;
            if (e.Index == 0)
            {
                bool isChecked = e.NewValue == CheckState.Checked;
                for (int ii = 1; ii < _spriteInfoList.Count + 1; ++ii)
                {
                    checkedListBox_Rotation.SetItemChecked(ii, isChecked);
                }
            }
            else
            {
                bool hasCheckedItems = false;
                bool hasUncheckedItems = false;
                for (int ii = 1; ii < _spriteInfoList.Count + 1; ++ii)
                {
                    CheckState checkedState = ii == e.Index ? e.NewValue : checkedListBox_Rotation.GetItemCheckState(ii);

                    if (checkedState == CheckState.Checked)
                    {
                        hasCheckedItems = true;
                    }
                    else
                    {
                        hasUncheckedItems = true;
                    }
                }

                CheckState checkState;
                if (hasCheckedItems && hasUncheckedItems)
                {
                    checkState = CheckState.Indeterminate;
                }
                else if (hasCheckedItems)
                {
                    checkState = CheckState.Checked;
                }
                else
                {
                    checkState = CheckState.Unchecked;
                }

                checkedListBox_Rotation.SetItemCheckState(0, checkState);
            }
            _suppressItemCheck = false;
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            // Lol hardcoded, i dont care
            switch (tabControlExportMode.SelectedIndex)
            {
                case 0:
                    textBoxExport.Text = CodeExport();
                    break;
                case 1:
                    textBoxExport.Text = CustomDataExport();
                    break;
                default:
                    // Lol how
                    Logger.Default.WriteLine($"Congrats! You managed to break my tab control! (index out of range: {tabControlExportMode.SelectedIndex})", Logger.Severity.Error);
                    break;
            }
        }
    }
}
