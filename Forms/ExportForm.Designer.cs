﻿namespace SpriteBuilder
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonExport = new System.Windows.Forms.Button();
            this.textBoxExport = new System.Windows.Forms.TextBox();
            this.tabControlExportMode = new System.Windows.Forms.TabControl();
            this.tabPageCode = new System.Windows.Forms.TabPage();
            this.checkBoxCsConstructor = new System.Windows.Forms.CheckBox();
            this.checkBoxCsColorScale = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxExportFunctionName = new System.Windows.Forms.TextBox();
            this.tabPageCustomData = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownScreenIndex = new System.Windows.Forms.NumericUpDown();
            this.checkedListBox_Rotation = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControlExportMode.SuspendLayout();
            this.tabPageCode.SuspendLayout();
            this.tabPageCustomData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Export Mode";
            // 
            // buttonExport
            // 
            this.buttonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExport.Location = new System.Drawing.Point(12, 395);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(180, 43);
            this.buttonExport.TabIndex = 2;
            this.buttonExport.Text = "Export";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // textBoxExport
            // 
            this.textBoxExport.Location = new System.Drawing.Point(209, 12);
            this.textBoxExport.Multiline = true;
            this.textBoxExport.Name = "textBoxExport";
            this.textBoxExport.ReadOnly = true;
            this.textBoxExport.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxExport.Size = new System.Drawing.Size(579, 426);
            this.textBoxExport.TabIndex = 3;
            this.textBoxExport.WordWrap = false;
            // 
            // tabControlExportMode
            // 
            this.tabControlExportMode.Controls.Add(this.tabPageCode);
            this.tabControlExportMode.Controls.Add(this.tabPageCustomData);
            this.tabControlExportMode.Location = new System.Drawing.Point(3, 25);
            this.tabControlExportMode.Name = "tabControlExportMode";
            this.tabControlExportMode.SelectedIndex = 0;
            this.tabControlExportMode.Size = new System.Drawing.Size(200, 364);
            this.tabControlExportMode.TabIndex = 5;
            // 
            // tabPageCode
            // 
            this.tabPageCode.Controls.Add(this.label4);
            this.tabPageCode.Controls.Add(this.checkedListBox_Rotation);
            this.tabPageCode.Controls.Add(this.checkBoxCsConstructor);
            this.tabPageCode.Controls.Add(this.checkBoxCsColorScale);
            this.tabPageCode.Controls.Add(this.label3);
            this.tabPageCode.Controls.Add(this.textBoxExportFunctionName);
            this.tabPageCode.Location = new System.Drawing.Point(4, 22);
            this.tabPageCode.Name = "tabPageCode";
            this.tabPageCode.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCode.Size = new System.Drawing.Size(192, 338);
            this.tabPageCode.TabIndex = 0;
            this.tabPageCode.Text = "C# Code";
            this.tabPageCode.UseVisualStyleBackColor = true;
            // 
            // checkBoxCsConstructor
            // 
            this.checkBoxCsConstructor.AutoSize = true;
            this.checkBoxCsConstructor.Location = new System.Drawing.Point(9, 73);
            this.checkBoxCsConstructor.Name = "checkBoxCsConstructor";
            this.checkBoxCsConstructor.Size = new System.Drawing.Size(129, 17);
            this.checkBoxCsConstructor.TabIndex = 8;
            this.checkBoxCsConstructor.Text = "Use sprite constructor";
            this.checkBoxCsConstructor.UseVisualStyleBackColor = true;
            // 
            // checkBoxCsColorScale
            // 
            this.checkBoxCsColorScale.AutoSize = true;
            this.checkBoxCsColorScale.Location = new System.Drawing.Point(9, 50);
            this.checkBoxCsColorScale.Name = "checkBoxCsColorScale";
            this.checkBoxCsColorScale.Size = new System.Drawing.Size(104, 17);
            this.checkBoxCsColorScale.TabIndex = 7;
            this.checkBoxCsColorScale.Text = "Color scale input";
            this.checkBoxCsColorScale.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Export Function Name";
            // 
            // textBoxExportFunctionName
            // 
            this.textBoxExportFunctionName.Location = new System.Drawing.Point(9, 24);
            this.textBoxExportFunctionName.Name = "textBoxExportFunctionName";
            this.textBoxExportFunctionName.Size = new System.Drawing.Size(178, 20);
            this.textBoxExportFunctionName.TabIndex = 5;
            this.textBoxExportFunctionName.Text = "DrawSprites";
            this.textBoxExportFunctionName.TextChanged += new System.EventHandler(this.textBoxExportFunctionName_TextChanged);
            // 
            // tabPageCustomData
            // 
            this.tabPageCustomData.Controls.Add(this.label2);
            this.tabPageCustomData.Controls.Add(this.numericUpDownScreenIndex);
            this.tabPageCustomData.Location = new System.Drawing.Point(4, 22);
            this.tabPageCustomData.Name = "tabPageCustomData";
            this.tabPageCustomData.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomData.Size = new System.Drawing.Size(192, 338);
            this.tabPageCustomData.TabIndex = 1;
            this.tabPageCustomData.Text = "Custom Data";
            this.tabPageCustomData.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Text surface index:";
            // 
            // numericUpDownScreenIndex
            // 
            this.numericUpDownScreenIndex.Location = new System.Drawing.Point(109, 6);
            this.numericUpDownScreenIndex.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownScreenIndex.Name = "numericUpDownScreenIndex";
            this.numericUpDownScreenIndex.Size = new System.Drawing.Size(38, 20);
            this.numericUpDownScreenIndex.TabIndex = 7;
            // 
            // checkedListBox_Rotation
            // 
            this.checkedListBox_Rotation.CheckOnClick = true;
            this.checkedListBox_Rotation.FormattingEnabled = true;
            this.checkedListBox_Rotation.Items.AddRange(new object[] {
            "(All)"});
            this.checkedListBox_Rotation.Location = new System.Drawing.Point(8, 111);
            this.checkedListBox_Rotation.Name = "checkedListBox_Rotation";
            this.checkedListBox_Rotation.Size = new System.Drawing.Size(176, 214);
            this.checkedListBox_Rotation.TabIndex = 9;
            this.checkedListBox_Rotation.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_Rotation_ItemCheck);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Rotation Input:";
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlExportMode);
            this.Controls.Add(this.textBoxExport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonExport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportForm";
            this.Text = "Whip\'s SE Sprite Builder - Export Menu";
            this.tabControlExportMode.ResumeLayout(false);
            this.tabPageCode.ResumeLayout(false);
            this.tabPageCode.PerformLayout();
            this.tabPageCustomData.ResumeLayout(false);
            this.tabPageCustomData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScreenIndex)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.TextBox textBoxExport;
        private System.Windows.Forms.TabControl tabControlExportMode;
        private System.Windows.Forms.TabPage tabPageCode;
        private System.Windows.Forms.TabPage tabPageCustomData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownScreenIndex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxExportFunctionName;
        private System.Windows.Forms.CheckBox checkBoxCsColorScale;
        private System.Windows.Forms.CheckBox checkBoxCsConstructor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox checkedListBox_Rotation;
    }
}