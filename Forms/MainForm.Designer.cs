﻿using System.Windows.Forms;

namespace SpriteBuilder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxSprites = new System.Windows.Forms.ListBox();
            this.contextMenuStripSpriteList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripSpriteType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.textureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxSpriteProps = new System.Windows.Forms.GroupBox();
            this.groupBoxTexture = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelNativeSize = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxSpriteImage = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownRotation = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSpriteSizeX = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDownSpriteSizeY = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonChangeSpriteColor = new System.Windows.Forms.Button();
            this.textBoxSpriteName = new System.Windows.Forms.TextBox();
            this.spriteTextAlignmentLabel = new System.Windows.Forms.Label();
            this.comboBoxAlignment = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownSpriteLocationY = new System.Windows.Forms.NumericUpDown();
            this.pictureBoxSpriteColor = new System.Windows.Forms.PictureBox();
            this.numericUpDownSpriteLocationX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxText = new System.Windows.Forms.GroupBox();
            this.numericUpDownSpriteTextScale = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxSpriteTextContent = new System.Windows.Forms.TextBox();
            this.groupBoxSpriteList = new System.Windows.Forms.GroupBox();
            this.pictureBoxCanvas = new System.Windows.Forms.PictureBox();
            this.groupBoxGeneralSettings = new System.Windows.Forms.GroupBox();
            this.comboBoxScreenSurface = new System.Windows.Forms.ComboBox();
            this.comboBoxScreenBlock = new System.Windows.Forms.ComboBox();
            this.checkBoxScreenOutline = new System.Windows.Forms.CheckBox();
            this.checkBoxShowSelection = new System.Windows.Forms.CheckBox();
            this.checkBoxGuideLines = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonChangeCanvasColor = new System.Windows.Forms.Button();
            this.pictureBoxCanvasColor = new System.Windows.Forms.PictureBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelSize = new System.Windows.Forms.Label();
            this.labelScreenOutlineSize = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToPNGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.locateSpaceEngineersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialogGameExecutable = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogPNG = new System.Windows.Forms.SaveFileDialog();
            this.contextMenuStripSpriteList.SuspendLayout();
            this.contextMenuStripSpriteType.SuspendLayout();
            this.groupBoxSpriteProps.SuspendLayout();
            this.groupBoxTexture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRotation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationX)).BeginInit();
            this.groupBoxText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteTextScale)).BeginInit();
            this.groupBoxSpriteList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvas)).BeginInit();
            this.groupBoxGeneralSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvasColor)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxSprites
            // 
            this.listBoxSprites.ContextMenuStrip = this.contextMenuStripSpriteList;
            this.listBoxSprites.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSprites.FormattingEnabled = true;
            this.listBoxSprites.Location = new System.Drawing.Point(3, 16);
            this.listBoxSprites.Name = "listBoxSprites";
            this.listBoxSprites.Size = new System.Drawing.Size(217, 493);
            this.listBoxSprites.TabIndex = 0;
            this.listBoxSprites.SelectedIndexChanged += new System.EventHandler(this.listBoxSprites_SelectedIndexChanged);
            this.listBoxSprites.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBoxSprites_MouseDown);
            // 
            // contextMenuStripSpriteList
            // 
            this.contextMenuStripSpriteList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.duplicateToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem});
            this.contextMenuStripSpriteList.Name = "contextMenuStripSpriteList";
            this.contextMenuStripSpriteList.Size = new System.Drawing.Size(181, 136);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addToolStripMenuItem.Text = "New...";
            // 
            // contextMenuStripSpriteType
            // 
            this.contextMenuStripSpriteType.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textureToolStripMenuItem,
            this.textToolStripMenuItem});
            this.contextMenuStripSpriteType.Name = "contextMenuStripSpriteType";
            this.contextMenuStripSpriteType.Size = new System.Drawing.Size(113, 48);
            // 
            // textureToolStripMenuItem
            // 
            this.textureToolStripMenuItem.Name = "textureToolStripMenuItem";
            this.textureToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.textureToolStripMenuItem.Text = "Texture";
            this.textureToolStripMenuItem.Click += new System.EventHandler(this.textureToolStripMenuItem_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // duplicateToolStripMenuItem
            // 
            this.duplicateToolStripMenuItem.Name = "duplicateToolStripMenuItem";
            this.duplicateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.duplicateToolStripMenuItem.Text = "Duplicate";
            this.duplicateToolStripMenuItem.Click += new System.EventHandler(this.duplicateToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removeToolStripMenuItem.Text = "Delete";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.moveUpToolStripMenuItem.Text = "Move up";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.moveUpToolStripMenuItem_Click);
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.moveDownToolStripMenuItem.Text = "Move down";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.moveDownToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDown = this.contextMenuStripSpriteList;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // groupBoxSpriteProps
            // 
            this.groupBoxSpriteProps.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSpriteProps.Controls.Add(this.groupBoxTexture);
            this.groupBoxSpriteProps.Controls.Add(this.buttonChangeSpriteColor);
            this.groupBoxSpriteProps.Controls.Add(this.textBoxSpriteName);
            this.groupBoxSpriteProps.Controls.Add(this.spriteTextAlignmentLabel);
            this.groupBoxSpriteProps.Controls.Add(this.comboBoxAlignment);
            this.groupBoxSpriteProps.Controls.Add(this.label3);
            this.groupBoxSpriteProps.Controls.Add(this.label4);
            this.groupBoxSpriteProps.Controls.Add(this.numericUpDownSpriteLocationY);
            this.groupBoxSpriteProps.Controls.Add(this.pictureBoxSpriteColor);
            this.groupBoxSpriteProps.Controls.Add(this.numericUpDownSpriteLocationX);
            this.groupBoxSpriteProps.Controls.Add(this.label1);
            this.groupBoxSpriteProps.Controls.Add(this.label7);
            this.groupBoxSpriteProps.Location = new System.Drawing.Point(12, 222);
            this.groupBoxSpriteProps.Name = "groupBoxSpriteProps";
            this.groupBoxSpriteProps.Size = new System.Drawing.Size(223, 281);
            this.groupBoxSpriteProps.TabIndex = 1;
            this.groupBoxSpriteProps.TabStop = false;
            this.groupBoxSpriteProps.Text = "Sprite Properties";
            // 
            // groupBoxTexture
            // 
            this.groupBoxTexture.Controls.Add(this.label9);
            this.groupBoxTexture.Controls.Add(this.labelNativeSize);
            this.groupBoxTexture.Controls.Add(this.label8);
            this.groupBoxTexture.Controls.Add(this.comboBoxSpriteImage);
            this.groupBoxTexture.Controls.Add(this.label2);
            this.groupBoxTexture.Controls.Add(this.numericUpDownRotation);
            this.groupBoxTexture.Controls.Add(this.numericUpDownSpriteSizeX);
            this.groupBoxTexture.Controls.Add(this.label11);
            this.groupBoxTexture.Controls.Add(this.numericUpDownSpriteSizeY);
            this.groupBoxTexture.Controls.Add(this.label6);
            this.groupBoxTexture.Location = new System.Drawing.Point(6, 131);
            this.groupBoxTexture.Name = "groupBoxTexture";
            this.groupBoxTexture.Size = new System.Drawing.Size(211, 141);
            this.groupBoxTexture.TabIndex = 10;
            this.groupBoxTexture.TabStop = false;
            this.groupBoxTexture.Text = "Texture";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Native Size";
            // 
            // labelNativeSize
            // 
            this.labelNativeSize.AutoSize = true;
            this.labelNativeSize.Location = new System.Drawing.Point(73, 107);
            this.labelNativeSize.Name = "labelNativeSize";
            this.labelNativeSize.Size = new System.Drawing.Size(27, 13);
            this.labelNativeSize.TabIndex = 22;
            this.labelNativeSize.Text = "N/A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(129, 76);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "x";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // comboBoxSpriteImage
            // 
            this.comboBoxSpriteImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpriteImage.FormattingEnabled = true;
            this.comboBoxSpriteImage.Location = new System.Drawing.Point(65, 13);
            this.comboBoxSpriteImage.Name = "comboBoxSpriteImage";
            this.comboBoxSpriteImage.Size = new System.Drawing.Size(140, 21);
            this.comboBoxSpriteImage.TabIndex = 14;
            this.comboBoxSpriteImage.SelectedIndexChanged += new System.EventHandler(this.comboBoxSpriteImage_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Size";
            // 
            // numericUpDownRotation
            // 
            this.numericUpDownRotation.Location = new System.Drawing.Point(65, 44);
            this.numericUpDownRotation.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownRotation.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownRotation.Name = "numericUpDownRotation";
            this.numericUpDownRotation.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownRotation.TabIndex = 20;
            this.numericUpDownRotation.ValueChanged += new System.EventHandler(this.numericUpDownRotation_ValueChanged);
            this.numericUpDownRotation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // numericUpDownSpriteSizeX
            // 
            this.numericUpDownSpriteSizeX.Location = new System.Drawing.Point(65, 74);
            this.numericUpDownSpriteSizeX.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.Name = "numericUpDownSpriteSizeX";
            this.numericUpDownSpriteSizeX.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteSizeX.TabIndex = 4;
            this.numericUpDownSpriteSizeX.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeX.ValueChanged += new System.EventHandler(this.numericUpDownSpriteSizeX_ValueChanged);
            this.numericUpDownSpriteSizeX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Rotation";
            // 
            // numericUpDownSpriteSizeY
            // 
            this.numericUpDownSpriteSizeY.Location = new System.Drawing.Point(144, 74);
            this.numericUpDownSpriteSizeY.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.Name = "numericUpDownSpriteSizeY";
            this.numericUpDownSpriteSizeY.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteSizeY.TabIndex = 5;
            this.numericUpDownSpriteSizeY.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericUpDownSpriteSizeY.ValueChanged += new System.EventHandler(this.numericUpDownSpriteSizeY_ValueChanged);
            this.numericUpDownSpriteSizeY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Sprite";
            // 
            // buttonChangeSpriteColor
            // 
            this.buttonChangeSpriteColor.Location = new System.Drawing.Point(105, 100);
            this.buttonChangeSpriteColor.Name = "buttonChangeSpriteColor";
            this.buttonChangeSpriteColor.Size = new System.Drawing.Size(87, 25);
            this.buttonChangeSpriteColor.TabIndex = 12;
            this.buttonChangeSpriteColor.Text = "Change Color";
            this.buttonChangeSpriteColor.UseVisualStyleBackColor = true;
            this.buttonChangeSpriteColor.Click += new System.EventHandler(this.buttonChangeSpriteColor_Click);
            // 
            // textBoxSpriteName
            // 
            this.textBoxSpriteName.Location = new System.Drawing.Point(71, 13);
            this.textBoxSpriteName.Name = "textBoxSpriteName";
            this.textBoxSpriteName.Size = new System.Drawing.Size(121, 20);
            this.textBoxSpriteName.TabIndex = 9;
            this.textBoxSpriteName.TextChanged += new System.EventHandler(this.textBoxSpriteName_TextChanged);
            this.textBoxSpriteName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // spriteTextAlignmentLabel
            // 
            this.spriteTextAlignmentLabel.AutoSize = true;
            this.spriteTextAlignmentLabel.Location = new System.Drawing.Point(6, 75);
            this.spriteTextAlignmentLabel.Name = "spriteTextAlignmentLabel";
            this.spriteTextAlignmentLabel.Size = new System.Drawing.Size(53, 13);
            this.spriteTextAlignmentLabel.TabIndex = 13;
            this.spriteTextAlignmentLabel.Text = "Alignment";
            // 
            // comboBoxAlignment
            // 
            this.comboBoxAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAlignment.FormattingEnabled = true;
            this.comboBoxAlignment.Location = new System.Drawing.Point(71, 72);
            this.comboBoxAlignment.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAlignment.Name = "comboBoxAlignment";
            this.comboBoxAlignment.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAlignment.TabIndex = 14;
            this.comboBoxAlignment.SelectedIndexChanged += new System.EventHandler(this.comboBoxAlignment_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Color";
            // 
            // numericUpDownSpriteLocationY
            // 
            this.numericUpDownSpriteLocationY.Location = new System.Drawing.Point(136, 44);
            this.numericUpDownSpriteLocationY.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteLocationY.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownSpriteLocationY.Name = "numericUpDownSpriteLocationY";
            this.numericUpDownSpriteLocationY.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteLocationY.TabIndex = 7;
            this.numericUpDownSpriteLocationY.ValueChanged += new System.EventHandler(this.numericUpDownSpriteLocationY_ValueChanged);
            this.numericUpDownSpriteLocationY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // pictureBoxSpriteColor
            // 
            this.pictureBoxSpriteColor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxSpriteColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSpriteColor.Location = new System.Drawing.Point(71, 100);
            this.pictureBoxSpriteColor.Name = "pictureBoxSpriteColor";
            this.pictureBoxSpriteColor.Size = new System.Drawing.Size(28, 25);
            this.pictureBoxSpriteColor.TabIndex = 10;
            this.pictureBoxSpriteColor.TabStop = false;
            // 
            // numericUpDownSpriteLocationX
            // 
            this.numericUpDownSpriteLocationX.Location = new System.Drawing.Point(71, 44);
            this.numericUpDownSpriteLocationX.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteLocationX.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDownSpriteLocationX.Name = "numericUpDownSpriteLocationX";
            this.numericUpDownSpriteLocationX.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteLocationX.TabIndex = 6;
            this.numericUpDownSpriteLocationX.ValueChanged += new System.EventHandler(this.numericUpDownSpriteLocationX_ValueChanged);
            this.numericUpDownSpriteLocationX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Position";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(126, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = ",";
            // 
            // groupBoxText
            // 
            this.groupBoxText.Controls.Add(this.numericUpDownSpriteTextScale);
            this.groupBoxText.Controls.Add(this.label12);
            this.groupBoxText.Controls.Add(this.label10);
            this.groupBoxText.Controls.Add(this.textBoxSpriteTextContent);
            this.groupBoxText.Location = new System.Drawing.Point(263, 353);
            this.groupBoxText.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxText.Name = "groupBoxText";
            this.groupBoxText.Size = new System.Drawing.Size(211, 141);
            this.groupBoxText.TabIndex = 9;
            this.groupBoxText.TabStop = false;
            this.groupBoxText.Text = "Text";
            // 
            // numericUpDownSpriteTextScale
            // 
            this.numericUpDownSpriteTextScale.DecimalPlaces = 2;
            this.numericUpDownSpriteTextScale.Location = new System.Drawing.Point(65, 74);
            this.numericUpDownSpriteTextScale.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSpriteTextScale.Name = "numericUpDownSpriteTextScale";
            this.numericUpDownSpriteTextScale.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownSpriteTextScale.TabIndex = 21;
            this.numericUpDownSpriteTextScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSpriteTextScale.ValueChanged += new System.EventHandler(this.numericUpDownSpriteTextScale_ValueChanged);
            this.numericUpDownSpriteTextScale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 76);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Scale";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Content";
            // 
            // textBoxSpriteTextContent
            // 
            this.textBoxSpriteTextContent.Location = new System.Drawing.Point(65, 13);
            this.textBoxSpriteTextContent.Multiline = true;
            this.textBoxSpriteTextContent.Name = "textBoxSpriteTextContent";
            this.textBoxSpriteTextContent.Size = new System.Drawing.Size(106, 55);
            this.textBoxSpriteTextContent.TabIndex = 10;
            this.textBoxSpriteTextContent.Text = "Text";
            this.textBoxSpriteTextContent.TextChanged += new System.EventHandler(this.textBoxSpriteTextContent_TextChanged);
            this.textBoxSpriteTextContent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SuppressEnterKeyDing);
            // 
            // groupBoxSpriteList
            // 
            this.groupBoxSpriteList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSpriteList.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSpriteList.Controls.Add(this.listBoxSprites);
            this.groupBoxSpriteList.Location = new System.Drawing.Point(759, 29);
            this.groupBoxSpriteList.Name = "groupBoxSpriteList";
            this.groupBoxSpriteList.Size = new System.Drawing.Size(223, 512);
            this.groupBoxSpriteList.TabIndex = 2;
            this.groupBoxSpriteList.TabStop = false;
            this.groupBoxSpriteList.Text = "Sprite List";
            // 
            // pictureBoxCanvas
            // 
            this.pictureBoxCanvas.BackColor = System.Drawing.Color.Black;
            this.pictureBoxCanvas.ContextMenuStrip = this.contextMenuStripSpriteType;
            this.pictureBoxCanvas.Location = new System.Drawing.Point(241, 29);
            this.pictureBoxCanvas.Name = "pictureBoxCanvas";
            this.pictureBoxCanvas.Size = new System.Drawing.Size(512, 512);
            this.pictureBoxCanvas.TabIndex = 3;
            this.pictureBoxCanvas.TabStop = false;
            this.pictureBoxCanvas.Click += new System.EventHandler(this.pictureBoxCanvas_Click);
            this.pictureBoxCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseDown);
            this.pictureBoxCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseMove);
            this.pictureBoxCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxCanvas_MouseUp);
            // 
            // groupBoxGeneralSettings
            // 
            this.groupBoxGeneralSettings.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxGeneralSettings.Controls.Add(this.comboBoxScreenSurface);
            this.groupBoxGeneralSettings.Controls.Add(this.comboBoxScreenBlock);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxScreenOutline);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxShowSelection);
            this.groupBoxGeneralSettings.Controls.Add(this.checkBoxGuideLines);
            this.groupBoxGeneralSettings.Controls.Add(this.label5);
            this.groupBoxGeneralSettings.Controls.Add(this.buttonChangeCanvasColor);
            this.groupBoxGeneralSettings.Controls.Add(this.pictureBoxCanvasColor);
            this.groupBoxGeneralSettings.Location = new System.Drawing.Point(12, 29);
            this.groupBoxGeneralSettings.Name = "groupBoxGeneralSettings";
            this.groupBoxGeneralSettings.Size = new System.Drawing.Size(223, 187);
            this.groupBoxGeneralSettings.TabIndex = 4;
            this.groupBoxGeneralSettings.TabStop = false;
            this.groupBoxGeneralSettings.Text = "Canvas Properties";
            // 
            // comboBoxScreenSurface
            // 
            this.comboBoxScreenSurface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScreenSurface.FormattingEnabled = true;
            this.comboBoxScreenSurface.Location = new System.Drawing.Point(27, 150);
            this.comboBoxScreenSurface.Name = "comboBoxScreenSurface";
            this.comboBoxScreenSurface.Size = new System.Drawing.Size(149, 21);
            this.comboBoxScreenSurface.TabIndex = 19;
            this.comboBoxScreenSurface.SelectedIndexChanged += new System.EventHandler(this.comboBoxScreenSurface_SelectedIndexChanged);
            // 
            // comboBoxScreenBlock
            // 
            this.comboBoxScreenBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScreenBlock.FormattingEnabled = true;
            this.comboBoxScreenBlock.Location = new System.Drawing.Point(27, 123);
            this.comboBoxScreenBlock.Name = "comboBoxScreenBlock";
            this.comboBoxScreenBlock.Size = new System.Drawing.Size(149, 21);
            this.comboBoxScreenBlock.TabIndex = 18;
            this.comboBoxScreenBlock.SelectedIndexChanged += new System.EventHandler(this.comboBoxScreenBlock_SelectedIndexChanged);
            // 
            // checkBoxScreenOutline
            // 
            this.checkBoxScreenOutline.AutoSize = true;
            this.checkBoxScreenOutline.Location = new System.Drawing.Point(6, 100);
            this.checkBoxScreenOutline.Name = "checkBoxScreenOutline";
            this.checkBoxScreenOutline.Size = new System.Drawing.Size(126, 17);
            this.checkBoxScreenOutline.TabIndex = 17;
            this.checkBoxScreenOutline.Text = "Show Screen Outline";
            this.checkBoxScreenOutline.UseVisualStyleBackColor = true;
            this.checkBoxScreenOutline.CheckedChanged += new System.EventHandler(this.checkBoxScreenOutline_CheckedChanged);
            // 
            // checkBoxShowSelection
            // 
            this.checkBoxShowSelection.AutoSize = true;
            this.checkBoxShowSelection.Checked = true;
            this.checkBoxShowSelection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShowSelection.Location = new System.Drawing.Point(6, 77);
            this.checkBoxShowSelection.Name = "checkBoxShowSelection";
            this.checkBoxShowSelection.Size = new System.Drawing.Size(121, 17);
            this.checkBoxShowSelection.TabIndex = 16;
            this.checkBoxShowSelection.Text = "Show Selection Box";
            this.checkBoxShowSelection.UseVisualStyleBackColor = true;
            this.checkBoxShowSelection.CheckedChanged += new System.EventHandler(this.checkBoxShowSelection_CheckedChanged);
            // 
            // checkBoxGuideLines
            // 
            this.checkBoxGuideLines.AutoSize = true;
            this.checkBoxGuideLines.Location = new System.Drawing.Point(6, 54);
            this.checkBoxGuideLines.Name = "checkBoxGuideLines";
            this.checkBoxGuideLines.Size = new System.Drawing.Size(112, 17);
            this.checkBoxGuideLines.TabIndex = 15;
            this.checkBoxGuideLines.Text = "Show Guide Lines";
            this.checkBoxGuideLines.UseVisualStyleBackColor = true;
            this.checkBoxGuideLines.CheckedChanged += new System.EventHandler(this.checkBoxGuideLines_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Color";
            // 
            // buttonChangeCanvasColor
            // 
            this.buttonChangeCanvasColor.Location = new System.Drawing.Point(80, 23);
            this.buttonChangeCanvasColor.Name = "buttonChangeCanvasColor";
            this.buttonChangeCanvasColor.Size = new System.Drawing.Size(87, 25);
            this.buttonChangeCanvasColor.TabIndex = 13;
            this.buttonChangeCanvasColor.Text = "Change Color";
            this.buttonChangeCanvasColor.UseVisualStyleBackColor = true;
            this.buttonChangeCanvasColor.Click += new System.EventHandler(this.buttonChangeCanvasColor_Click);
            // 
            // pictureBoxCanvasColor
            // 
            this.pictureBoxCanvasColor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxCanvasColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxCanvasColor.Location = new System.Drawing.Point(46, 23);
            this.pictureBoxCanvasColor.Name = "pictureBoxCanvasColor";
            this.pictureBoxCanvasColor.Size = new System.Drawing.Size(28, 25);
            this.pictureBoxCanvasColor.TabIndex = 0;
            this.pictureBoxCanvasColor.TabStop = false;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "NewProject.xml";
            this.saveFileDialog.Filter = "XML files|*.xml|All files|*.*";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "XML files|*.xml|All files|*.*";
            // 
            // labelSize
            // 
            this.labelSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(59, 526);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(117, 13);
            this.labelSize.TabIndex = 6;
            this.labelSize.Text = "Canvas size: 512 x 512";
            // 
            // labelScreenOutlineSize
            // 
            this.labelScreenOutlineSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelScreenOutlineSize.AutoSize = true;
            this.labelScreenOutlineSize.Location = new System.Drawing.Point(46, 507);
            this.labelScreenOutlineSize.Name = "labelScreenOutlineSize";
            this.labelScreenOutlineSize.Size = new System.Drawing.Size(149, 13);
            this.labelScreenOutlineSize.TabIndex = 7;
            this.labelScreenOutlineSize.Text = "Screen outline size: 512 x 512";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(990, 24);
            this.menuStrip.TabIndex = 8;
            this.menuStrip.Text = "menuStrip";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportToolStripMenuItem,
            this.exportToPNGToolStripMenuItem,
            this.toolStripSeparator2,
            this.locateSpaceEngineersToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.optionsToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.newToolStripMenuItem.Text = "New                                                          Ctrl + N";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.openToolStripMenuItem.Text = "Open                                                        Ctrl + O";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveToolStripMenuItem.Text = "Save                                                          Ctrl + S";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveAsToolStripMenuItem.Text = "Save As                                        Ctrl + Shift + S";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(311, 6);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.exportToolStripMenuItem.Text = "Export                                                       Ctrl + E";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // exportToPNGToolStripMenuItem
            // 
            this.exportToPNGToolStripMenuItem.Name = "exportToPNGToolStripMenuItem";
            this.exportToPNGToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.exportToPNGToolStripMenuItem.Text = "Export to PNG";
            this.exportToPNGToolStripMenuItem.Click += new System.EventHandler(this.exportToPNGToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(311, 6);
            // 
            // locateSpaceEngineersToolStripMenuItem
            // 
            this.locateSpaceEngineersToolStripMenuItem.Name = "locateSpaceEngineersToolStripMenuItem";
            this.locateSpaceEngineersToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.locateSpaceEngineersToolStripMenuItem.Text = "Locate Space Engineers ";
            this.locateSpaceEngineersToolStripMenuItem.Click += new System.EventHandler(this.locateGameExecutableToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.ToolTipText = "Open Sprite Builder wiki in browser";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(990, 545);
            this.Controls.Add(this.groupBoxText);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.labelScreenOutlineSize);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.groupBoxGeneralSettings);
            this.Controls.Add(this.pictureBoxCanvas);
            this.Controls.Add(this.groupBoxSpriteList);
            this.Controls.Add(this.groupBoxSpriteProps);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(560, 584);
            this.Name = "MainForm";
            this.Text = "Sprite Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.contextMenuStripSpriteList.ResumeLayout(false);
            this.contextMenuStripSpriteType.ResumeLayout(false);
            this.groupBoxSpriteProps.ResumeLayout(false);
            this.groupBoxSpriteProps.PerformLayout();
            this.groupBoxTexture.ResumeLayout(false);
            this.groupBoxTexture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRotation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteSizeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpriteColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteLocationX)).EndInit();
            this.groupBoxText.ResumeLayout(false);
            this.groupBoxText.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSpriteTextScale)).EndInit();
            this.groupBoxSpriteList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvas)).EndInit();
            this.groupBoxGeneralSettings.ResumeLayout(false);
            this.groupBoxGeneralSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCanvasColor)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxSprites;
        private System.Windows.Forms.GroupBox groupBoxSpriteProps;
        private System.Windows.Forms.GroupBox groupBoxSpriteList;
        private System.Windows.Forms.PictureBox pictureBoxCanvas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxGeneralSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxCanvasColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.PictureBox pictureBoxSpriteColor;
        private System.Windows.Forms.TextBox textBoxSpriteName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteLocationY;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteLocationX;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteSizeY;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteSizeX;
        private System.Windows.Forms.Button buttonChangeSpriteColor;
        private System.Windows.Forms.Button buttonChangeCanvasColor;
        private System.Windows.Forms.CheckBox checkBoxGuideLines;
        private System.Windows.Forms.ComboBox comboBoxSpriteImage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownRotation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxShowSelection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.GroupBox groupBoxText;
        private System.Windows.Forms.NumericUpDown numericUpDownSpriteTextScale;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxSpriteTextContent;
        private System.Windows.Forms.GroupBox groupBoxTexture;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxScreenOutline;
        private System.Windows.Forms.ComboBox comboBoxScreenSurface;
        private System.Windows.Forms.ComboBox comboBoxScreenBlock;
        private System.Windows.Forms.Label labelScreenOutlineSize;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialogGameExecutable;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.Label labelNativeSize;
        private System.Windows.Forms.ToolStripMenuItem exportToPNGToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialogPNG;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem locateSpaceEngineersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxAlignment;
        private System.Windows.Forms.Label spriteTextAlignmentLabel;
        private Label label8;
        private Label label9;
        private ContextMenuStrip contextMenuStripSpriteList;
        private ToolStripMenuItem addToolStripMenuItem;
        private ToolStripMenuItem duplicateToolStripMenuItem;
        private ToolStripMenuItem removeToolStripMenuItem;
        private ToolStripMenuItem moveUpToolStripMenuItem;
        private ToolStripMenuItem moveDownToolStripMenuItem;
        private ToolStripMenuItem editToolStripMenuItem;
        private ContextMenuStrip contextMenuStripSpriteType;
        private ToolStripMenuItem textureToolStripMenuItem;
        private ToolStripMenuItem textToolStripMenuItem;
    }
}

