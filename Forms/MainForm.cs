using Newtonsoft.Json.Linq;
using Pfim;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Whiplash.Utils;

namespace SpriteBuilder
{
    public partial class MainForm : Form
    {
        Color CanvasColor
        {
            get
            {
                return _canvasColor;
            }
            set
            {
                _canvasColor = value;
                pictureBoxCanvasColor.BackColor = _canvasColor;
            }
        }

        SpriteInfo SelectedSpriteInfo
        {
            get
            {
                int index = listBoxSprites.SelectedIndex;
                if (index < 0 || index >= _spriteInfoList.Count)
                {
                    return null;
                }
                return _spriteInfoList[index];
            }
        }

        private string _gameExecutablePath = null;
        string GameExecutablePath
        {
            get
            {
                return _gameExecutablePath;
            }
            set
            {
                if (value != _gameExecutablePath)
                {
                    _gameExecutablePath = value;
                    _gameFolderPath = FoundGameExecutable ? Directory.GetParent(value).Parent.FullName : "";
                    LoadSpriteImages();
                    locateSpaceEngineersToolStripMenuItem.Enabled = !FoundGameExecutable;
                    locateSpaceEngineersToolStripMenuItem.Text = FoundGameExecutable ? "Space Engineers Found" : "Locate Space Engineers";
                    locateSpaceEngineersToolStripMenuItem.ToolTipText = FoundGameExecutable ? $"Found Space Engineers at {_gameExecutablePath}" : "Click to locate Space Engineers executable";
                }
            }
        }

        private string _gameFolderPath;
        string GameFolderPath
        {
            get 
            {
                return _gameFolderPath;
            }
        }


        bool FoundGameExecutable
        {
            get
            {
                return File.Exists(GameExecutablePath);
            }
        }

        const double MinUpdateInterval = 1.0 / 60.0;
        bool _inTimeout = false;

        Vector2 _lastMouseDownLocation;
        Color _canvasColor,
            _guideLineColor,
            _selectionLineColor,
            _screenOutlineColor;

        string _currentFile;
        string CurrentFile
        {
            get
            {
                return _currentFile;
            }
            set
            {
                if (value != _currentFile)
                {
                    _currentFile = value;
                    _currentFileExists = File.Exists(_currentFile);
                    if (_currentFileExists)
                    {
                        string directory = Directory.GetParent(_currentFile).FullName;
                        saveFileDialog.InitialDirectory = openFileDialog.InitialDirectory = saveFileDialogPNG.InitialDirectory = directory;
                        saveFileDialog.FileName = openFileDialog.FileName = Path.GetFileName(_currentFile);
                        saveFileDialogPNG.FileName = Path.GetFileNameWithoutExtension(_currentFile) + ".png";
                    }
                    saveToolStripMenuItem.Enabled = _currentFileExists;
                }
            }
        }

        bool _currentFileExists = false;
        bool CurrentFileExists
        {
            get
            {
                return _currentFileExists;
            }
        }

        List<SpriteInfo> _spriteInfoList = new List<SpriteInfo>();
        Dictionary<string, Bitmap> _spriteImageMap = new Dictionary<string, Bitmap>();
        Dictionary<string, string> _spriteFilePathMap = new Dictionary<string, string>();
        HashSet<string> _usedSprites = new HashSet<string>();
        HashSet<string> _baseSprites = new HashSet<string>();
        List<string> _spritesToUncache = new List<string>();


        List<string> _availableSprites = new List<string>();
        int _textureSpriteCount = 0;
        int _textSpriteCount = 0;
        bool _switchingSelectedSprite = false;

        const float SelectionLineWidth = 4f;
        const float SelectionBoxPadding = 16f;
        const int CanvasPadding = 4;

        bool _mouseDownInCanvas = false;
        FormWindowState _lastFormWindowState = FormWindowState.Normal;
        FontFamily _seFontFamily;

        readonly List<GCHandle> _gcHandles = new List<GCHandle>();
        readonly List<string> _screenBlockNames = new List<string>();
        Vector2 _screenOutlineSize = new Vector2(0, 0);

        public MainForm()
        {
            Logger.CreateDefault("SpriteBuilder.log", "Sprite Builder");
            Logger.Default.WriteLine($"App Version: v{Constants.MyVersionString}");

            InitializeComponent();

            this.Text = $"Whip's SE Sprite Builder - {Constants.MyVersionString}";
            Task updateCheck = CheckForUpdates();

            openFileDialogGameExecutable.Filter = "Executable files (*.exe)|*.exe";

            saveFileDialogPNG.Filter = "PNG files (*.png)|*.png";
            saveFileDialogPNG.DefaultExt = ".png";

            _guideLineColor = Color.FromArgb(90, 190, 255);
            _selectionLineColor = Color.FromArgb(255, 242, 0);
            _screenOutlineColor = Color.FromArgb(255, 127, 39);
            CanvasColor = Color.Black;
            pictureBoxSpriteColor.BackColor = Color.White;

            SearchForGameExecutable();
            PopulateComboBoxes();

            comboBoxScreenBlock.Enabled = checkBoxScreenOutline.Checked;
            comboBoxScreenSurface.Enabled = checkBoxScreenOutline.Checked;
            labelScreenOutlineSize.Visible = checkBoxScreenOutline.Checked;

            _seFontFamily = Utilities.LoadFont(Constants.SpaceEngineersFontName);

            groupBoxText.Parent = groupBoxSpriteProps;
            groupBoxText.Location = groupBoxTexture.Location;
            groupBoxText.Visible = false;

            // There is a lovely bug where if you bind this in the designer, the first time either the contextMenuStripSpriteType or
            // contextMenuStripSpriteList is opened via an object with a ContextMenuList binding, the context menu will be spawned in
            // the wrong place. Screen 0,0 for contextMenuStripSpriteType and the edit button drop down location for
            // contextMenuStripSpriteList.
            addToolStripMenuItem.DropDown = contextMenuStripSpriteType;

            TryDrawCanvas();
        }

        void SearchForGameExecutable()
        {
            Logger.Default.WriteLine("Searching for game executable...");
            foreach (string pathBase in Constants.ExecutableSearchPaths)
            {
                for (int ii = 0; ii < Constants.DriveCharacters.Length; ++ii) 
                {
                    var path = string.Format(pathBase, Constants.DriveCharacters[ii], Constants.SpaceEngineersExecutableName);
                    if (File.Exists(path))
                    {
                        GameExecutablePath = path;
                        Logger.Default.WriteLine($"Found game executable at: {GameExecutablePath}");
                        Logger.Default.WriteLine($"Game folder at: {GameFolderPath}");
                        return;
                    }
                }
            }
            Logger.Default.WriteLine($"Count not find game executable");
            GameExecutablePath = "";
        }

        void PopulateComboBoxes()
        {
            foreach (var tsp in TextSurfaceProvider.TextSurfaceProviders)
            {
                _screenBlockNames.Add(tsp.BlockName);
            }

            // screenBlockNames.Add("(Custom)");

            comboBoxScreenBlock.DataSource = _screenBlockNames;
            comboBoxScreenBlock.SelectedIndex = 0;

            List<string> alignments = new List<string>(Enum.GetNames(typeof(SpriteAlignment)));

            comboBoxAlignment.DataSource = alignments.Skip(1).ToList();

            SelectTextSurfaceTypeComboBoxes(0);
        }

        void SelectTextSurfaceTypeComboBoxes(int index)
        {
            comboBoxScreenSurface.Items.Clear();

            if (index < TextSurfaceProvider.TextSurfaceProviders.Count)
            {
                foreach (var surf in TextSurfaceProvider.TextSurfaceProviders[index].TextSurfaces)
                {
                    comboBoxScreenSurface.Items.Add(surf.SurfaceName);
                }
            }
            else
            {
                comboBoxScreenSurface.Items.Add("N/A");
            }

            comboBoxScreenSurface.SelectedIndex = 0;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Logger.Default.Close();
            foreach(GCHandle handle in _gcHandles)
            {
                handle.Free();
            }
        }

        private bool TryGetLCDTextureSubtype(XmlNode idNode, out string subtype)
        {
            XmlNode subtypeNode;
            if ((subtypeNode = idNode.SelectSingleNode("SubtypeId")) != null)
            {
                subtype = subtypeNode.InnerText;
                return true;
            }
            
            foreach (XmlAttribute attr in idNode.Attributes)
            {
                if (attr.Name == "Subtype")
                {
                    subtype = attr.InnerText;
                    return true;
                }
            }

            subtype = "";
            return false;
        }

        private bool TryGetLCDTextureFileName(XmlNode lcdTextureNode, out string path)
        {
            path = null;

            XmlNode spritePathNode = lcdTextureNode.SelectSingleNode("SpritePath");
            if (spritePathNode == null)
            {
                return false;
            }

            path = Path.Combine(GameFolderPath, "Content", spritePathNode.InnerText);
            if (!File.Exists(path))
            {
                Logger.Default.WriteLine($"SpritePath '{path}' is not a valid file", Logger.Severity.Warning);
                return false;
            }

            return true;
        }

        private bool EnsureLoaded(in string subtype)
        {
            if (_spriteImageMap.ContainsKey(subtype))
            {
                return true;
            }
            
            if (_spriteFilePathMap.TryGetValue(subtype, out string path))
            {
                if (TryGetLCDTextureBitmap(path, out Bitmap bitmap))
                {
                    _spriteImageMap.Add(subtype, bitmap);
                    return true;
                }
            }

            return false;
        }

        private bool TryGetLCDTextureBitmap(in string path, out Bitmap image)
        {
            image = null;

            if (path.EndsWith(".dds", StringComparison.OrdinalIgnoreCase))
            {
                var img = Pfimage.FromFile(path);

                PixelFormat format;
                switch (img.Format)
                {
                    case Pfim.ImageFormat.Rgb24:
                        format = PixelFormat.Format24bppRgb;
                        break;

                    case Pfim.ImageFormat.Rgba32:
                        format = PixelFormat.Format32bppArgb;
                        break;

                    case Pfim.ImageFormat.R5g5b5:
                        format = PixelFormat.Format16bppRgb555;
                        break;

                    case Pfim.ImageFormat.R5g6b5:
                        format = PixelFormat.Format16bppRgb565;
                        break;

                    case Pfim.ImageFormat.R5g5b5a1:
                        format = PixelFormat.Format16bppArgb1555;
                        break;

                    case Pfim.ImageFormat.Rgb8:
                        format = PixelFormat.Format8bppIndexed;
                        break;

                    default:
                        var msg = $"{img.Format} is not recognized for Bitmap on Windows Forms. " +
                                    "You'd need to write a conversion function to convert the data to known format";
                        Logger.Default.WriteLine(msg, Logger.Severity.Warning);
                        return false;
                }

                var handle = GCHandle.Alloc(img.Data, GCHandleType.Pinned);
                _gcHandles.Add(handle);
                try
                {
                    var ptr = Marshal.UnsafeAddrOfPinnedArrayElement(img.Data, 0);
                    image = new Bitmap(img.Width, img.Height, img.Stride, format, ptr);
                    return true;
                }
                catch (Exception e)
                {
                    Logger.Default.WriteLine($"Failed to read DDS image: {path}\n{e}", Logger.Severity.Error);
                    return false;
                }
            }

            try
            {
                image = new Bitmap(path);
                return true;
            }
            catch (Exception e)
            {
                Logger.Default.WriteLine($"Failed to read non-DDS image: {path}\n{e}", Logger.Severity.Error);
                return false;
            }
        }

        private bool TryParseLCDTextureDefinitionNode(XmlNode lcdTextureNode, out string subtype, out string imagePath)
        {
            subtype = "";
            imagePath = null;

            if (lcdTextureNode.Name != "LCDTextureDefinition")
            {
                return false;
            }

            XmlNode idNode = lcdTextureNode.SelectSingleNode("Id");
            if (idNode == null)
            {
                return false;
            }

            if (!TryGetLCDTextureSubtype(idNode, out subtype))
            {
                return false;
            }

            return TryGetLCDTextureFileName(lcdTextureNode, out imagePath);
        }

        private void LoadSpriteImages()
        {
            _spriteImageMap.Clear();
            _availableSprites.Clear();

            _spriteImageMap["SquareSimple"] = Properties.Resources.SquareSimple;
            _spriteImageMap["SquareHollow"] = Properties.Resources.SquareHollow;
            _spriteImageMap["Circle"] = Properties.Resources.Circle;
            _spriteImageMap["CircleHollow"] = Properties.Resources.CircleHollow;
            _spriteImageMap["SemiCircle"] = Properties.Resources.SemiCircle;
            _spriteImageMap["Triangle"] = Properties.Resources.Triangle;
            _spriteImageMap["RightTriangle"] = Properties.Resources.RightTriangle;

            foreach (var kvp in _spriteImageMap)
            {
                _availableSprites.Add(kvp.Key);
                _baseSprites.Add(kvp.Key);
            }

            if (FoundGameExecutable)
            { 
                string contentFolder = Path.Combine(GameFolderPath, "Content", "Data");

                foreach (string lcdTextureFile in Directory.EnumerateFiles(contentFolder).Where(f => Path.GetFileName(f).StartsWith("LCDTextures")))
                {
                    Logger.Default.WriteLine($"Processing LCD Texture file: {lcdTextureFile}");

                    if (File.Exists(lcdTextureFile))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(lcdTextureFile);

                        XmlNode texturesNode = doc.DocumentElement.SelectSingleNode("/Definitions/LCDTextures");

                        foreach (XmlNode node in texturesNode.ChildNodes)
                        {
                            if (TryParseLCDTextureDefinitionNode(node, out string subtype, out string imagePath))
                            {
                                Logger.Default.WriteLine($"Found sprite: {subtype}");
                                _spriteFilePathMap[subtype] = imagePath;

                                if (!_availableSprites.Contains(subtype))
                                {
                                    _availableSprites.Add(subtype);
                                }
                            }
                        }
                    }
                }
            }

            comboBoxSpriteImage.DataSource = null;
            comboBoxSpriteImage.DataSource = _availableSprites;
        }

        private void buttonChangeCanvasColor_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                CanvasColor = colorDialog.Color;
                TryDrawCanvas();
            }
        }

        private void checkBoxGuideLines_CheckedChanged(object sender, EventArgs e)
        {
            TryDrawCanvas();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys keys = keyData & Keys.KeyCode;
            Keys modifiers = keyData & Keys.Modifiers;
            int increment = 1;

            // Speed up changex x5 is control is pressed
            if ((modifiers & Keys.Control) != 0)
                increment = 5;

            if ((keyData == (Keys.Control | Keys.Shift | Keys.S)))
            {
                SaveAs();
                return true;
            }

            if ((keyData == (Keys.Control | Keys.S)))
            {
                Save();
                return true;
            }

            if ((keyData == (Keys.Control | Keys.N)))
            {
                New();
                return true;
            }

            if ((keyData == (Keys.Control | Keys.O)))
            {
                Open();
                return true;
            }

            if ((keyData == (Keys.Control | Keys.E)))
            {
                Export();
                return true;
            }

            if (this.ActiveControl == pictureBoxCanvas && SelectedSpriteInfo != null)
            {
                if ((modifiers & Keys.Shift) == 0) // Shift not pressed -> translation
                {
                    switch (keys)
                    {
                        case Keys.Up:
                            numericUpDownSpriteLocationY.Value -= increment;
                            return true;

                        case Keys.Down:
                            numericUpDownSpriteLocationY.Value += increment;
                            return true;

                        case Keys.Left:
                            numericUpDownSpriteLocationX.Value -= increment;
                            return true;

                        case Keys.Right:
                            numericUpDownSpriteLocationX.Value += increment;
                            return true;
                    }
                }
                else // Shift pressed -> rotation
                {
                    switch (keys)
                    {
                        case Keys.Left:
                            numericUpDownRotation.Value -= increment;
                            return true;

                        case Keys.Right:
                            numericUpDownRotation.Value += increment;
                            return true;
                    }
                }
            }

            if(this.ActiveControl == listBoxSprites)
            {
                switch (keys)
                {
                    case Keys.Up:
                        MoveSpriteUp();
                        return true;

                    case Keys.Down:
                        MoveSpriteDown();
                        return true;
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void DrawSelectionBox(Graphics graphics, Vector2 position, Vector2 size, float rotationDeg, SpriteType type)
        {
            // Compute corners
            Vector2 halfSize = size * 0.5f + Vector2.One * 0.5f * (SelectionLineWidth + SelectionBoxPadding);
            Vector2 topLeft;
            if (type == SpriteType.TEXTURE)
            {
                topLeft = position - halfSize;
            }
            else
            {
                topLeft = position - (Vector2.One * 0.5f * (SelectionLineWidth + SelectionBoxPadding));
                rotationDeg = 0;
            }
            Pen pen = new Pen(_selectionLineColor, SelectionLineWidth);

            Rectangle r = new Rectangle(
                (int)(topLeft.X + 0.5f),
                (int)(topLeft.Y + 0.5f),
                (int)Math.Round(2f * halfSize.X),
                (int)Math.Round(2f * halfSize.Y));

            using (Matrix m = new Matrix())
            {
                m.RotateAt(rotationDeg, new PointF(r.Left + (r.Width / 2),
                                          r.Top + (r.Height / 2)));
                graphics.Transform = m;
                graphics.DrawRectangle(pen, r);
                graphics.ResetTransform();
            }
            pen.Dispose();
        }

        async Task StartDrawTimeout()
        {
            await Task.Delay((int)(MinUpdateInterval * 1000));
            DrawCanvas();
            _inTimeout = false;
        }

        Task _timeoutTask;
        private void TryDrawCanvas()
        {
            if (!_inTimeout)
            {
                _timeoutTask?.Dispose();
                _inTimeout = true;
                _timeoutTask = StartDrawTimeout();
            }
        }

        void DrawCanvas()
        {
            _usedSprites.Clear();

            Bitmap bitmap = new Bitmap(pictureBoxCanvas.Width, pictureBoxCanvas.Height);
            Graphics canvasGraphics = Graphics.FromImage(bitmap);
            canvasGraphics.Clear(_canvasColor);

            bool showGuideLines = checkBoxGuideLines.Checked;
            bool showSelection = checkBoxShowSelection.Checked;
            bool showScreenSizeOutline = checkBoxScreenOutline.Checked;
            Vector2 canvasSize = new Vector2(pictureBoxCanvas.Size.Width, pictureBoxCanvas.Size.Height);
            Vector2 midPoint = canvasSize * 0.5f;

            // Iterate from bottom to top drawing our sprites
            for (int i = _spriteInfoList.Count - 1; i >= 0; --i)
            {
                SpriteInfo spriteInfo = _spriteInfoList[i];

                if (spriteInfo.Type == SpriteType.TEXTURE)
                {
                    if (!EnsureLoaded(spriteInfo.SpriteType))
                    {
                        Logger.Default.WriteLine($"Could not load sprite \"{spriteInfo.SpriteType}\" when drawing canvas", Logger.Severity.Warning);
                        continue;
                    }

                    _usedSprites.Add(spriteInfo.SpriteType);

                    Bitmap baseImage = _spriteImageMap[spriteInfo.SpriteType];
                    Size size = spriteInfo.Size.ToSize();
                    using (Bitmap scaledImage = Utilities.ResizeImage(baseImage, size))
                    using (Bitmap rotatedAndScaledImage = Utilities.RotateImage(scaledImage, spriteInfo.Rotation))
                    using (ImageAttributes coloredImageAttributes = Utilities.GetRecolorImageAttributes(rotatedAndScaledImage, spriteInfo.Color.ToColor()))
                    {
                        Vector2 rotatedSizeVec = new Vector2(rotatedAndScaledImage.Size.Width, rotatedAndScaledImage.Size.Height);
                        Vector2 locationVec = midPoint + spriteInfo.Position - 0.5f * rotatedSizeVec + spriteInfo.GetAlignmentOffset();
                        Point location = locationVec.ToPoint();
                        canvasGraphics.DrawImage(
                            rotatedAndScaledImage,
                            new Rectangle(location.X, location.Y , rotatedAndScaledImage.Width, rotatedAndScaledImage.Height),
                            0, 0,
                            rotatedAndScaledImage.Width,
                            rotatedAndScaledImage.Height,
                            GraphicsUnit.Pixel,
                            coloredImageAttributes);
                    }
                }
                else if (spriteInfo.Type == SpriteType.TEXT)
                {
                    using (Font font = new Font(_seFontFamily, spriteInfo.Rotation * Constants.FontScaleToPx, GraphicsUnit.Pixel))
                    using (SolidBrush brush = new SolidBrush(spriteInfo.Color.ToColor()))
                    {
                        //var scaledOffset = Constants.TextPositionOffset * -spriteInfo.Rotation;
                        
                        SizeF size = canvasGraphics.MeasureString(spriteInfo.SpriteType, font);
                        spriteInfo.Size = new Vector2(size.Width, size.Height);
                        canvasGraphics.DrawString(spriteInfo.SpriteType, font, brush,
                                    midPoint.X + spriteInfo.Position.X + spriteInfo.GetAlignmentOffset().X,
                                    midPoint.Y + spriteInfo.Position.Y + spriteInfo.GetAlignmentOffset().Y);
                        
                    }
                }
            }

            if (showGuideLines)
            {
                Point topCenter = (midPoint - Vector2.UnitY * midPoint.Y).ToPoint();
                Point bottomCenter = (midPoint + Vector2.UnitY * midPoint.Y).ToPoint();
                Point leftCenter = (midPoint - Vector2.UnitX * midPoint.X).ToPoint();
                Point rightCenter = (midPoint + Vector2.UnitX * midPoint.X).ToPoint();
                Pen pen = new Pen(_guideLineColor);
                pen.DashStyle = DashStyle.Dash;
                canvasGraphics.DrawLine(pen, topCenter, bottomCenter);
                canvasGraphics.DrawLine(pen, leftCenter, rightCenter);
                pen.Dispose();
            }

            // Draw selection
            if (showSelection && SelectedSpriteInfo != null)
            {
                DrawSelectionBox(canvasGraphics, midPoint + SelectedSpriteInfo.Position + SelectedSpriteInfo.GetAlignmentOffset(),
                            SelectedSpriteInfo.Size, SelectedSpriteInfo.Rotation, SelectedSpriteInfo.Type);
            }

            if (showScreenSizeOutline)
            {
                using (Pen pen = new Pen(_screenOutlineColor))
                {
                    pen.DashStyle = DashStyle.Dash;

                    Vector2 topLeft = midPoint - 0.5f * _screenOutlineSize;
                    Rectangle r = new Rectangle(
                        (int)(topLeft.X),
                        (int)(topLeft.Y),
                        (int)Math.Round(_screenOutlineSize.X),
                        (int)Math.Round(_screenOutlineSize.Y));

                    canvasGraphics.DrawRectangle(pen, r);
                }
            }

            _spritesToUncache.Clear();

            // Cull unused sprites
            foreach (string subtype in _spriteImageMap.Keys)
            {
                if (!_baseSprites.Contains(subtype) && !_usedSprites.Contains(subtype))
                {
                    _spritesToUncache.Add(subtype);
                    
                }
            }

            foreach (string subtype in _spritesToUncache)
            {
                Bitmap image = _spriteImageMap[subtype];
                image.Dispose();
                _spriteImageMap.Remove(subtype);
                Logger.Default.WriteLine($"Uncached image: {subtype}");
            }

            Image oldImage = pictureBoxCanvas.Image;
            pictureBoxCanvas.Image = bitmap;
            canvasGraphics.Dispose();
            if (oldImage != null)
                oldImage.Dispose();

        }

        void UpdateNativeSpriteSize()
        {
            if (SelectedSpriteInfo.Type == SpriteType.TEXTURE)
            {
                if (EnsureLoaded(SelectedSpriteInfo.SpriteType) && _spriteImageMap.TryGetValue(SelectedSpriteInfo.SpriteType, out Bitmap image))
                {
                    labelNativeSize.Text = $"{image.Width} x {image.Height}";
                }
            }
            else
            {
                labelNativeSize.Text = $"N/A";
            }
        }

        private void UpdateSelectedSpriteDisplayData()
        {
            textBoxSpriteName.Text = SelectedSpriteInfo.Name;
            numericUpDownSpriteLocationX.Value = (decimal)SelectedSpriteInfo.Position.X;
            numericUpDownSpriteLocationY.Value = (decimal)SelectedSpriteInfo.Position.Y;
            pictureBoxSpriteColor.BackColor = SelectedSpriteInfo.Color.ToColor();

            UpdateNativeSpriteSize();

            switch (SelectedSpriteInfo.Type)
            {
                case SpriteType.TEXTURE:
                    for (int i = 0; i < comboBoxSpriteImage.Items.Count; ++i)
                    {
                        if (comboBoxSpriteImage.Items[i].ToString() == SelectedSpriteInfo.SpriteType)
                        {
                            comboBoxSpriteImage.SelectedIndex = i;
                            break;
                        }
                    }
                    numericUpDownSpriteSizeX.Value = (decimal)SelectedSpriteInfo.Size.X;
                    numericUpDownSpriteSizeY.Value = (decimal)SelectedSpriteInfo.Size.Y;
                    numericUpDownRotation.Value = (decimal)SelectedSpriteInfo.Rotation;
                    groupBoxText.Visible = false;
                    groupBoxTexture.Visible = true;
                    break;
                case SpriteType.TEXT:
                    textBoxSpriteTextContent.Text = SelectedSpriteInfo.SpriteType;
                    numericUpDownSpriteTextScale.Value = (decimal)SelectedSpriteInfo.Rotation;
                    groupBoxText.Visible = true;
                    groupBoxTexture.Visible = false;
                    break;
            }

            comboBoxAlignment.SelectedIndex = (int)SelectedSpriteInfo.Alignment - 1;
        }

        private void listBoxSprites_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null)
            {
                Logger.Default.WriteLine("Sprite index out of bounds");
                return;
            }
 
            _switchingSelectedSprite = true;
            UpdateSelectedSpriteDisplayData();
            _switchingSelectedSprite = false;
            TryDrawCanvas();
        }

        private void comboBoxSpriteImage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;

            string spriteType = comboBoxSpriteImage.SelectedItem.ToString();
            SelectedSpriteInfo.SpriteType = spriteType;
            UpdateNativeSpriteSize();
            TryDrawCanvas();
        }

        private void numericUpDownSpriteLocationX_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Position.X = (float)numericUpDownSpriteLocationX.Value;
            TryDrawCanvas();
        }

        private void numericUpDownSpriteLocationY_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Position.Y = (float)numericUpDownSpriteLocationY.Value;
            TryDrawCanvas();
        }

        private void numericUpDownSpriteSizeX_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Size.X = (float)numericUpDownSpriteSizeX.Value;
            TryDrawCanvas();
        }

        private void numericUpDownSpriteSizeY_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Size.Y = (float)numericUpDownSpriteSizeY.Value;
            TryDrawCanvas();
        }

        private void buttonChangeSpriteColor_Click(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null)
                return;
            var result = colorDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                SelectedSpriteInfo.Color = new SerializableColor(colorDialog.Color);
                pictureBoxSpriteColor.BackColor = colorDialog.Color;
            }
            TryDrawCanvas();
        }

        private void textBoxSpriteName_TextChanged(object sender, EventArgs e)
        {
            // var cursorPosition = textBoxSpriteName.SelectionStart;
            // textBoxSpriteName.Text = Regex.Replace(textBoxSpriteName.Text, "[^0-9a-zA-Z]", "");
            // textBoxSpriteName.SelectionStart = cursorPosition;

            if (SelectedSpriteInfo == null)
                return;
            string text = textBoxSpriteName.Text;
            SelectedSpriteInfo.Name = text;
            listBoxSprites.Items[listBoxSprites.SelectedIndex] = text;
        }

        public string SaveToXml()
        {
            SpriteContent content = new SpriteContent(CanvasColor, _spriteInfoList);
            return Utilities.SerializeToString(content);
        }

        void New()
        {
            if (listBoxSprites.Items.Count == 0)
            {
                return;
            }

            var result = MessageBox.Show(
                $"Are you sure you want to start a new sprite?{Environment.NewLine}Any unsaved progress will be lost.",
                "Confirm: New Sprite",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button2);

            if (result == DialogResult.Yes)
            {
                _spriteInfoList.Clear();
                listBoxSprites.Items.Clear();
                CurrentFile = null;
                TryDrawCanvas();
            }
        }

        void SaveAs()
        {
            var result = saveFileDialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }
            string xml = SaveToXml();
            File.WriteAllText(saveFileDialog.FileName, xml);
            CurrentFile = saveFileDialog.FileName;
            Logger.Default.WriteLine($"Sprites saved to XML file: {saveFileDialog.FileName}");
        }

        void Save()
        {
            if (CurrentFileExists)
            {
                string xml = SaveToXml();
                File.WriteAllText(CurrentFile, xml);
            }
        }

        void SaveToPNG()
        {
            var result = saveFileDialogPNG.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }


            bool showSelectionBox = checkBoxShowSelection.Checked;
            bool showGuidelines = checkBoxGuideLines.Checked;
            bool showScreenOutline = checkBoxScreenOutline.Checked;

            checkBoxShowSelection.Checked = checkBoxGuideLines.Checked = checkBoxScreenOutline.Checked = false;

            DrawCanvas();

            pictureBoxCanvas.Image.Save(saveFileDialogPNG.FileName);
            Logger.Default.WriteLine($"Saved sprites to image: {saveFileDialogPNG.FileName}");

            checkBoxShowSelection.Checked = showSelectionBox;
            checkBoxGuideLines.Checked = showGuidelines;
            checkBoxScreenOutline.Checked = showScreenOutline;
        }

        void Open()
        {
            var result = openFileDialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }

            string text = File.ReadAllText(openFileDialog.FileName);
            CurrentFile = openFileDialog.FileName;

            _spriteInfoList.Clear();
            listBoxSprites.Items.Clear();
            bool successfulLoad = true;
            if (Utilities.TryDeserializeFromString(text, out SpriteContent spriteContentDeserialized))
            {
                CanvasColor = spriteContentDeserialized.BackgroundColor.ToColor();
                foreach (var s in spriteContentDeserialized.Sprites)
                {
                    _spriteInfoList.Add(s);
                    listBoxSprites.Items.Add(s.Name);
                }
                Logger.Default.WriteLine("Loaded sprites from serialized SpriteContent");
            }
            else if (Utilities.TryDeserializeFromString(text, out List<SpriteInfo> spriteInfoListDeserialized))
            {
                // For backwards compatibility
                foreach (var s in spriteInfoListDeserialized)
                {
                    _spriteInfoList.Add(s);
                    listBoxSprites.Items.Add(s.Name);
                }
                Logger.Default.WriteLine("Loaded sprites from serialized SpriteInfo array");
            }
            else
            {
                successfulLoad = false;
            }

            if (listBoxSprites.Items.Count != 0)
                listBoxSprites.SelectedIndex = 0;
            if (successfulLoad)
            {
                Logger.Default.WriteLine($"Loaded sprites from XML file: {openFileDialog.FileName}");
            }
            else
            {
                Logger.Default.WriteLine($"Failed to load sprites from XML file: {openFileDialog.FileName}!", Logger.Severity.Warning);
            }
            TryDrawCanvas();
        }

        void Export()
        {
            ExportForm exportForm = new ExportForm(_spriteInfoList, CanvasColor);
            exportForm.ShowDialog();
        }

        private void buttonSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            New();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            Export();
        }

        private void checkBoxShowSelection_CheckedChanged(object sender, EventArgs e)
        {
            TryDrawCanvas();
        }

        private bool CheckMouseWithinSelectionBox(Point mouseLocation)
        {
            if (SelectedSpriteInfo == null)
                return false;

            Vector2 canvasSize = new Vector2(pictureBoxCanvas.Size.Width, pictureBoxCanvas.Size.Height);
            Vector2 midPoint = canvasSize * 0.5f;

            Vector2 size = SelectedSpriteInfo.Size + Vector2.One * (2f * SelectionLineWidth + SelectionBoxPadding);
            Vector2 halfSize = size * 0.5f;

            float angleDeg;
            Vector2 spritePosition;
            Vector2 offset = SelectedSpriteInfo.GetAlignmentOffset();
            if (SelectedSpriteInfo.Type == SpriteType.TEXTURE)
            {
                angleDeg = -SelectedSpriteInfo.Rotation;
                spritePosition = SelectedSpriteInfo.Position + offset; // TODO: Will need to account for rotation
            }
            else
            {
                angleDeg = 0;
                spritePosition = SelectedSpriteInfo.Position + SelectedSpriteInfo.Size * 0.5f + Constants.TextPositionOffset * -SelectedSpriteInfo.Rotation + offset;
            }
            Matrix2x2 rotMatrix = Matrix2x2.CreateFromRotationDeg(angleDeg);
            Vector2 transformedSpritePosition = rotMatrix.Transform(spritePosition);
            Vector2 transformedMousePosition = rotMatrix.Transform(new Vector2(mouseLocation.X, mouseLocation.Y) - midPoint);
            Vector2 diff = transformedMousePosition - transformedSpritePosition;

            bool withinBox = Math.Abs(diff.X) <= halfSize.X && Math.Abs(diff.Y) <= halfSize.Y;
            return withinBox;
        }

        private void pictureBoxCanvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            if (!CheckMouseWithinSelectionBox(e.Location))
                return;

            _mouseDownInCanvas = true;
            if (SelectedSpriteInfo == null)
                return;

            _lastMouseDownLocation = new Vector2(e.Location.X, e.Location.Y);
        }

        private void pictureBoxCanvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_mouseDownInCanvas)
                return;
            _mouseDownInCanvas = false;
            if (SelectedSpriteInfo == null)
                return;

            Vector2 mouseUpLocation = new Vector2(e.Location.X, e.Location.Y);
            Vector2 diff = mouseUpLocation - _lastMouseDownLocation;
            
            SelectedSpriteInfo.Position += diff;
            TryDrawCanvas();
            UpdateSelectedSpriteDisplayData();
        }
       
        private void pictureBoxCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (SelectedSpriteInfo == null || !_mouseDownInCanvas)
                return;

            Vector2 mouseMoveLocation = new Vector2(e.Location.X, e.Location.Y);
            Vector2 diff = mouseMoveLocation - _lastMouseDownLocation;

            SelectedSpriteInfo.Position += diff;
            if (diff != Vector2.Zero)
            {
                TryDrawCanvas();
            }
            _lastMouseDownLocation = mouseMoveLocation;
        }

        void MoveSpriteUp()
        {
            if (SelectedSpriteInfo == null)
                return;
            int idx = listBoxSprites.SelectedIndex;
            var spriteInfo = _spriteInfoList[idx];
            // Remove from current index
            _spriteInfoList.RemoveAt(idx);
            listBoxSprites.Items.RemoveAt(idx);
            // Re-insert an index ahead
            int adjustedIdx = idx == 0 ? 0 : idx - 1;
            _spriteInfoList.Insert(adjustedIdx, spriteInfo);
            listBoxSprites.Items.Insert(adjustedIdx, spriteInfo.Name);
            listBoxSprites.SelectedIndex = adjustedIdx;

            TryDrawCanvas();
        }

        void MoveSpriteDown()
        {
            if (SelectedSpriteInfo == null)
                return;
            int idx = listBoxSprites.SelectedIndex;
            var spriteInfo = _spriteInfoList[idx];
            // Remove from current index
            _spriteInfoList.RemoveAt(idx);
            listBoxSprites.Items.RemoveAt(idx);
            // Re-insert an index behind
            int adjustedIdx = idx == listBoxSprites.Items.Count ? idx : idx + 1;
            _spriteInfoList.Insert(adjustedIdx, spriteInfo);
            listBoxSprites.Items.Insert(adjustedIdx, spriteInfo.Name);
            listBoxSprites.SelectedIndex = adjustedIdx;

            TryDrawCanvas();
        }

        private void moveUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveSpriteUp();
        }

        private void moveDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoveSpriteDown();
        }

        private void numericUpDownRotation_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Rotation = (float)numericUpDownRotation.Value;

            TryDrawCanvas();
        }

        private void pictureBoxCanvas_Click(object sender, EventArgs e)
        {
            this.ActiveControl = pictureBoxCanvas;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            int targetHeight = this.ClientSize.Height - menuStrip.Size.Height - CanvasPadding * 2;
            int targetWidth = groupBoxSpriteList.Location.X - (groupBoxGeneralSettings.Location.X + groupBoxGeneralSettings.Size.Width) - CanvasPadding * 2;
            // Ensure that canvas size is even, otherwise rounding shifts stuff around
            pictureBoxCanvas.Height = targetHeight % 2 == 0 ? targetHeight : targetHeight - 1;
            pictureBoxCanvas.Width = targetWidth % 2 == 0 ? targetWidth : targetWidth - 1;
            groupBoxSpriteList.Height = targetHeight;
            labelSize.Text = $"Canvas size: {pictureBoxCanvas.Width} x {pictureBoxCanvas.Height}";

            if (WindowState != _lastFormWindowState)
            {
                Logger.Default.WriteLine($"Window state changed: {WindowState}");
                TryDrawCanvas();
            }
            _lastFormWindowState = WindowState;
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            Logger.Default.WriteLine($"Window resized to {Size}");
            TryDrawCanvas();
        }

        private void numericUpDownSpriteTextScale_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Rotation = (float)numericUpDownSpriteTextScale.Value;

            TryDrawCanvas();
        }

        private void textBoxSpriteTextContent_TextChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.SpriteType = textBoxSpriteTextContent.Text;

            TryDrawCanvas();
        }

        private void comboBoxScreenBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxScreenBlock.SelectedIndex < TextSurfaceProvider.TextSurfaceProviders.Count)
            {
                SelectTextSurfaceTypeComboBoxes(comboBoxScreenBlock.SelectedIndex);
            }
        }

        private void comboBoxScreenSurface_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxScreenBlock.SelectedIndex < TextSurfaceProvider.TextSurfaceProviders.Count)
            {
                _screenOutlineSize = TextSurfaceProvider.
                    TextSurfaceProviders[comboBoxScreenBlock.SelectedIndex].
                    TextSurfaces[comboBoxScreenSurface.SelectedIndex].SurfaceSize;
                labelScreenOutlineSize.Text = $"Screen outline size: {_screenOutlineSize.X} x {_screenOutlineSize.Y}";
            }
            TryDrawCanvas();
        }

        private void checkBoxScreenOutline_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxScreenBlock.Enabled = checkBoxScreenOutline.Checked;
            comboBoxScreenSurface.Enabled = checkBoxScreenOutline.Checked;
            labelScreenOutlineSize.Visible = checkBoxScreenOutline.Checked;
            TryDrawCanvas();
        }

        private void locateGameExecutableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = openFileDialogGameExecutable.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }

            string path = openFileDialogGameExecutable.FileName;
            if (!path.EndsWith(Constants.SpaceEngineersExecutableName) || !File.Exists(path))
            {
                MessageBox.Show(
                    "Game executable not found!",
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                Logger.Default.WriteLine($"Path '{path}' is not the space engineers executable", Logger.Severity.Warning);
                return;
            }

            GameExecutablePath = path;
        }

        private void exportToPNGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveToPNG();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.ProjectWikiUrl);
        }

        private void comboBoxAlignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null || _switchingSelectedSprite)
                return;
            SelectedSpriteInfo.Alignment = (SpriteAlignment)(comboBoxAlignment.SelectedIndex + 1);

            TryDrawCanvas();
        }

        Vector2 GetNewSpritePosition(object caller)
        {
            if (caller == pictureBoxCanvas)
            {
                Point location = pictureBoxCanvas.PointToClient(contextMenuStripSpriteType.Location);
                return location.ToVector2() - 0.5f * pictureBoxCanvas.Size.ToVector2();
            }
            else
            {
                return Vector2.Zero;
            }
        }

        private void textureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name = $"{Constants.GenericTextureSpriteName}{++_textureSpriteCount}";
            var spriteInfo = new SpriteInfo(name, _availableSprites[0], SpriteType.TEXTURE);
            spriteInfo.Position = GetNewSpritePosition(contextMenuStripSpriteType.SourceControl);
            Logger.Default.WriteLine($"Added texture sprite: {name}");
            _spriteInfoList.Insert(0, spriteInfo);
            listBoxSprites.Items.Insert(0, name);
            listBoxSprites.SelectedIndex = 0;

            TryDrawCanvas();
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name = $"{Constants.GenericTextSpriteName}{++_textSpriteCount}";
            var spriteInfo = new SpriteInfo(name, "Text", SpriteType.TEXT);
            spriteInfo.Position = GetNewSpritePosition(contextMenuStripSpriteType.SourceControl);
            Logger.Default.WriteLine($"Added text sprite: {name}");
            _spriteInfoList.Insert(0, spriteInfo);
            listBoxSprites.Items.Insert(0, name);
            listBoxSprites.SelectedIndex = 0;
            
            TryDrawCanvas();
        }

        private void duplicateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null)
                return;
            string copyName = $"{SelectedSpriteInfo.Name}Copy";
            var spriteInfo = new SpriteInfo()
            {
                Name = copyName,
                SpriteType = SelectedSpriteInfo.SpriteType,
                Position = SelectedSpriteInfo.Position,
                Size = SelectedSpriteInfo.Size,
                Color = SelectedSpriteInfo.Color,
                Rotation = SelectedSpriteInfo.Rotation,
                Type = SelectedSpriteInfo.Type,
                Font = SelectedSpriteInfo.Font,
            };
            Logger.Default.WriteLine($"Copied: {SelectedSpriteInfo.Name} to {copyName}");
            _spriteInfoList.Insert(listBoxSprites.SelectedIndex + 1, spriteInfo);
            listBoxSprites.Items.Insert(listBoxSprites.SelectedIndex + 1, copyName);
            listBoxSprites.SelectedIndex = listBoxSprites.SelectedIndex + 1;

            TryDrawCanvas();
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedSpriteInfo == null)
                return;

            var result = MessageBox.Show("Are you sure you want to remove this sprite?",
                                            "Confirm: Remove Sprite",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Warning);
            if (result != DialogResult.Yes)
            {
                return;
            }

            int idx = listBoxSprites.SelectedIndex;
            listBoxSprites.Items.RemoveAt(idx);
            _spriteInfoList.RemoveAt(idx);
            if (_spriteInfoList.Count > 0)
            {
                idx = Math.Min(idx, _spriteInfoList.Count - 1);
                listBoxSprites.SelectedIndex = idx;
            }
            TryDrawCanvas();
        }

        private void listBoxSprites_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var newIndex = listBoxSprites.IndexFromPoint(e.Location);
                if (newIndex >= 0)
                {
                    listBoxSprites.SelectedIndex = newIndex;
                }
            }
        }

        /// <summary>
        /// The enter key generates an annoying "ding" noise when you hit it while inside a single
        /// line text box or a numeric up down control. This will stop that from happening.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SuppressEnterKeyDing(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && sender != textBoxSpriteTextContent)
            {
                e.SuppressKeyPress = true;
            }
        }

        // Source: https://stackoverflow.com/a/27108442
        async Task<string> GetWebRequest(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        async Task CheckForUpdates()
        {
            try
            {
                var apiResponse = await GetWebRequest(Constants.ProjectApiUrl);
                JArray releases = JArray.Parse(apiResponse);
                if (releases.Count <= 0)
                {
                    Logger.Default.WriteLine("No releases in API response", Logger.Severity.Warning);
                    return;
                }
                JObject mostRecentRelease = releases[0] as JObject;
                if (mostRecentRelease == null)
                {
                    Logger.Default.WriteLine("Most recent release is null", Logger.Severity.Warning);
                    return;
                }
                if (!mostRecentRelease.TryGetValue("tag_name", out JToken releaseToken))
                {
                    Logger.Default.WriteLine("tag_name token is not present", Logger.Severity.Warning);
                    return;
                }
                string latestReleaseString = releaseToken.Value<string>();
                latestReleaseString = latestReleaseString.ToLower().Replace("v", "");

                Version latestVersion = new Version();
                if (!Version.TryParse(latestReleaseString, out latestVersion))
                {
                    Logger.Default.WriteLine($"Failed to parse release version string: '{latestReleaseString}'", Logger.Severity.Warning);
                    return;
                }

                Version myVersion = new Version();
                if (!Version.TryParse(Constants.MyVersionString, out myVersion))
                {
                    Logger.Default.WriteLine($"Failed to parse app version string: '{Constants.MyVersionString}'", Logger.Severity.Warning);
                    return;
                }

                if (latestVersion > myVersion)
                {
                    Logger.Default.WriteLine($"App update found: v{latestReleaseString}");
                    var confirmResult = MessageBox.Show($"Old version detected. Update to newest version?\nYour version: {Constants.MyVersionString}\nLatest release: {latestReleaseString}",
                        "NEW UPDATE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                    if (confirmResult == DialogResult.Yes)
                    {
                        if (mostRecentRelease.TryGetValue("_links", out JToken linkToken)
                            && linkToken is JObject
                            && ((JObject)linkToken).TryGetValue("self", out JToken selfToken))
                        {
                            string linkUrl = selfToken.Value<string>();
                            System.Diagnostics.Process.Start(linkUrl);
                        }
                        else
                        {
                            System.Diagnostics.Process.Start(Constants.ProjectReleaseUrl);
                        }
                        Logger.Default.WriteLine("Navigating to webpage and closing app...");
                        this.Close();
                    }
                }
                else
                {
                    Logger.Default.WriteLine("App is up to date");
                }
            }
            catch (Exception e)
            {
                Logger.Default.WriteLine($"Update check failed!\n{e}", Logger.Severity.Warning);
                return;
            }
        }
    }
}
