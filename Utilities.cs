﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Numerics;
using System.Xml.Serialization;
using Whiplash.Utils;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;

namespace SpriteBuilder
{
    public static class Utilities
    {
        private static Dictionary<string, FontFamily> _fontFamilies = new Dictionary<string, FontFamily>();

        public static FontFamily LoadFont(string filename)
        {
            if (_fontFamilies.TryGetValue(filename, out FontFamily family))
            {
                return family;
            }

            using (Stream fontStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(filename))
            {
                IntPtr data = Marshal.AllocCoTaskMem((int)fontStream.Length);
                byte[] fontdata = new byte[fontStream.Length];
                fontStream.Read(fontdata, 0, (int)fontStream.Length);
                Marshal.Copy(fontdata, 0, data, (int)fontStream.Length);
                using (PrivateFontCollection privateFonts = new PrivateFontCollection())
                {
                    privateFonts.AddMemoryFont(data, (int)fontStream.Length);
                    family = privateFonts.Families[0];
                    _fontFamilies[filename] = family;
                }
                Marshal.FreeCoTaskMem(data);
            }
            return family;
        }

        public static bool TryDeserializeFromString<T>(string xml, out T obj)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (StringReader textReader = new StringReader(xml))
            using (XmlTextReader xmlReader = new XmlTextReader(textReader))
            {
                bool canDeserialize = xmlSerializer.CanDeserialize(xmlReader);
                if (canDeserialize)
                {
                    obj = (T)xmlSerializer.Deserialize(xmlReader);
                }
                else
                {
                    obj = default(T);
                }
                return canDeserialize;
            }
        }

        public static string SerializeToString<T>(T item)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(item.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, item);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Method to rotate an image either clockwise or counter-clockwise
        /// Source: https://stackoverflow.com/a/2163854
        /// </summary>
        /// <param name="img">the image to be rotated</param>
        /// <param name="rotationAngle">the angle (in degrees).
        /// NOTE: 
        /// Positive values will rotate clockwise
        /// negative values will rotate counter-clockwise
        /// </param>
        /// <returns></returns>
        public static Bitmap RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image with plenty of padding
            int max = Math.Max(img.Width, img.Height);
            Bitmap enlargedImg = new Bitmap(max * 2, max * 2);
            using (var g = Graphics.FromImage(enlargedImg))
            {
                g.DrawImage(img, (max - img.Width / 2), (max - img.Height / 2) , img.Width, img.Height);
            }

            Bitmap bmp = new Bitmap(enlargedImg.Width, enlargedImg.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(
                enlargedImg, 
                0, 
                0,
                enlargedImg.Width,
                enlargedImg.Height);

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// Source: https://stackoverflow.com/a/24199315
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, Size size)
        {
            int width = size.Width;
            int height = size.Height;
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static ImageAttributes GetRecolorImageAttributes(Image image, Color desiredColor)
        {
            ImageAttributes imageAttributes = new ImageAttributes();
            float[][] colorMatrixElements = {
                new float[] { desiredColor.R / 255f,  0,  0,  0, 0}, // R
                new float[] {0, desiredColor.G / 255f,  0,  0, 0}, // G
                new float[] {0,  0, desiredColor.B/255f,  0, 0}, // B
                new float[] {0,  0,  0,  1, 0}, // A
                new float[] {0, 0, 0, 0, 1} // Translation
            };
            ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);
            imageAttributes.SetColorMatrix(
               colorMatrix,
               ColorMatrixFlag.Default,
               ColorAdjustType.Bitmap);
            return imageAttributes;
        }
    }
}
