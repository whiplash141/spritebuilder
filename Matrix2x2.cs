﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SpriteBuilder
{
    struct Matrix2x2
    {
        public float M11;
        public float M12;
        public float M21;
        public float M22;

        static Matrix2x2()
        {
            Identity = new Matrix2x2
            {
                M11 = 1,
                M12 = 0,
                M21 = 0,
                M22 = 1,
            };
        }

        public Matrix2x2(float m11, float m12, float m21, float m22)
        {
            M11 = m11;
            M12 = m12;
            M21 = m21;
            M22 = m22;
        }

        /// <summary>
        /// Identity 2x2 matrix.
        /// </summary>
        public static Matrix2x2 Identity;

        /// <summary>
        /// Creates a rotation matrix from a clockwise angle in radians.
        /// </summary>
        /// <param name="rotationRad"></param>
        /// <returns></returns>
        public static Matrix2x2 CreateFromRotationRad(float rotationRad)
        {
            float cos = (float)Math.Cos(rotationRad);
            float sin = (float)Math.Sin(rotationRad);
            return new Matrix2x2(cos, -sin, sin, cos);
        }

        /// <summary>
        /// Creates a rotation matrix from a clockwise angle in degrees.
        /// </summary>
        /// <param name="rotationDeg"></param>
        /// <returns></returns>
        public static Matrix2x2 CreateFromRotationDeg(float rotationDeg)
        {
            float rotationRad = rotationDeg / 180f * (float)Math.PI;
            return CreateFromRotationRad(rotationRad);
        }

        /// <summary>
        /// Transposes matrix about its diagonal.
        /// </summary>
        /// <returns></returns>
        public Matrix2x2 Transpose()
        {
            return new Matrix2x2(M11, M21, M12, M22);
        }

        /// <summary>
        /// Transforms a Vector2 by this matrix.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public Vector2 Transform(Vector2 vec)
        {
            return new Vector2(vec.X * M11 + vec.Y * M12, vec.X * M21 + vec.Y * M22);
        }

        public override string ToString()
        {
            return string.Format("[[{0}, {1}],\n [{2}, {3}]]", M11, M12, M21, M22);
        }
    }
}
