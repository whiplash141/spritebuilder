﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SpriteBuilder
{
    internal static class Extensions
    {
        public static Point ToPoint(this Vector2 vec)
        {
            return new Point
            {
                X = (int)Math.Round(vec.X),
                Y = (int)Math.Round(vec.Y)
            };
        }

        public static Size ToSize(this Vector2 vec)
        {
            return new Size
            {
                Width = (int)Math.Round(vec.X),
                Height = (int)Math.Round(vec.Y)
            };
        }

        public static Vector2 ToVector2(this Point point)
        {
            return new Vector2
            {
                X = point.X,
                Y = point.Y
            };
        }

        public static Vector2 ToVector2(this Size size)
        {
            return new Vector2
            {
                X = size.Width,
                Y = size.Height
            };
        }
    }
}
