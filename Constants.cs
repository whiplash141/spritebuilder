﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SpriteBuilder
{
    static class Constants
    {
        public const string MyVersionString = "1.9.0";

        public const string ProjectApiUrl = "https://gitlab.com/api/v4/projects/22603626/releases";

        public const string ProjectWikiUrl = "https://gitlab.com/whiplash141/spritebuilder/-/wikis/home";

        public const string ProjectReleaseUrl = "https://gitlab.com/whiplash141/spritebuilder/-/releases";

        public const string DriveCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static readonly string[] ExecutableSearchPaths = {
            @"{0}:\Program Files (x86)\Steam\steamapps\common\SpaceEngineers\Bin64\{1}",
            @"{0}:\SteamLibrary\steamapps\common\SpaceEngineers\Bin64\{1}",
        };

        public const string DefaultExecutableSearchPath = @"C:\";

        public const string SpaceEngineersExecutableName = "SpaceEngineers.exe";

        public static readonly Vector2 TextPositionOffset = new Vector2(4, -1);

        public const float FontScaleToPx = 25.12f;

        public const float MagicTextOffsetPx = 13f;

        public const string SpaceEngineersFontName = "SpriteBuilder.Resources.space_engineers.ttf";

        public const string AllowedFunctionNamePattern = @"^[_A-Za-z]+[A-Za-z_0-9]*$";

        public const string GenericTextureSpriteName = "sprite";
        public const string GenericTextSpriteName = "text";

        public const string SpriteFunctionTemplate =
@"public void SetupDrawSurface(IMyTextSurface surface)
{{
    // Draw background color
    surface.ScriptBackgroundColor = new Color({0}, {1}, {2}, {3});

    // Set content type
    surface.ContentType = ContentType.SCRIPT;

    // Set script to none
    surface.Script = """";
}}

public void {4}(MySpriteDrawFrame frame, Vector2 centerPos, float scale = 1f{5})
{{
{6}}}
";

        public const string SpriteFunctionTemplateWithRotation =
@"public void SetupDrawSurface(IMyTextSurface surface)
{{
    // Draw background color
    surface.ScriptBackgroundColor = new Color({0}, {1}, {2}, {3});

    // Set content type
    surface.ContentType = ContentType.SCRIPT;

    // Set script to none
    surface.Script = """";
}}

public void {4}(MySpriteDrawFrame frame, Vector2 centerPos, float scale = 1f, float rotation = 0f{5})
{{
    float sin = (float)Math.Sin(rotation);
    float cos = (float)Math.Cos(rotation);
{6}}}
";
        public const string SpriteFunctionTemplateWithRotationNoTrig =
@"public void SetupDrawSurface(IMyTextSurface surface)
{{
    // Draw background color
    surface.ScriptBackgroundColor = new Color({0}, {1}, {2}, {3});

    // Set content type
    surface.ContentType = ContentType.SCRIPT;

    // Set script to none
    surface.Script = """";
}}

public void {4}(MySpriteDrawFrame frame, Vector2 centerPos, float scale = 1f, float rotation = 0f{5})
{{
{6}}}
";

        public const string SpriteCodeTemplate =
@"    frame.Add(new MySprite()
    {{
        Type = SpriteType.TEXTURE,
        Alignment = TextAlignment.{10},
        Data = ""{0}"",
        Position = new Vector2({1})*scale+centerPos,
        Size = new Vector2({2}f,{3}f)*scale,
        Color = new Color({4},{5},{6},{7}),
        RotationOrScale = {8}
    }}); // {9}";

        public const string TextSpriteCodeTemplate =
@"    frame.Add(new MySprite()
    {{
        Type = SpriteType.TEXT,
        Alignment = TextAlignment.{9},
        Data = ""{0}"",
        Position = new Vector2({1})*scale+centerPos,
        Color = new Color({2},{3},{4},{5}),
        FontId = ""{6}"",
        RotationOrScale = {7}*scale
    }}); // {8}";

        public const string TextureSpriteCodeConstructorTemplate = @"    frame.Add(new MySprite(SpriteType.TEXTURE, ""{0}"", new Vector2({1})*scale+centerPos, new Vector2({2}f,{3}f)*scale, new Color({4},{5},{6},{7}), null, TextAlignment.{10}, {8})); // {9}";
        public const string TextSpriteCodeConstructorTemplate = @"    frame.Add(new MySprite(SpriteType.TEXT, ""{0}"", new Vector2({1})*scale+centerPos, null, new Color({2},{3},{4},{5}), ""{6}"", TextAlignment.{9}, {7}*scale)); // {8}";

        public const string SpriteVectorFormat = @"{0}f,{1}f";
        public const string SpriteVectorFormatWithRotation = @"cos*{0}f-sin*{1}f,sin*{0}f+cos*{1}f";
        public const string SpriteVectorFormatWithRotationZeroX = @"-sin*{1}f,cos*{1}f";
        public const string SpriteVectorFormatWithRotationZeroY = @"cos*{0}f,sin*{0}f";

        public const string SpriteRotationFormat = @"{0:0.####}f";
        public const string SpriteRotationFormatWithRotation = @"{0:0.####}f+rotation";
        public const string SpriteRotationFormatWithRotationZero = @"rotation";

        public const string CustomDataTextureSpritePrefix = "Texture";
        public const string CustomDataTextSpritePrefix = "Text";


        public const string CustomDataScreenTemplate =
@"[Sprite:{0}]
Sprite location=
Sprite scale=1
Sprite color scale=1
Background color={1}, {2}, {3}, {4}
Sprite list={5}
{6}";

        public const string CustomDataTextureSpriteTemplate =
"\r\n["+ CustomDataTextureSpritePrefix+":{0}]"+
@"
Type={1}
Position={{X:{2} Y:{3}}}
Size={{X:{4} Y:{5}}}
Color={6}, {7}, {8}, {9}
Rotation={10}
Alignment={11}";

        public const string TextCustomDataSpriteTemplate =
"\r\n[" + CustomDataTextSpritePrefix + ":{0}]" +
@"
Text={1}
Position={{X:{2} Y:{3}}}
Color={4}, {5}, {6}, {7}
Font={8}
Scale={9}
Alignment={10}";

    }
}
