SPACE ENGINEERS SPRITE BUILDER

=========================================

This is a simple app for designing sprites in Space Engineers.
See the full wiki here: https://gitlab.com/whiplash141/spritebuilder/-/wikis/home


ATTRIBUTIONS

=========================================

Pertinent licences live in the ^/Licenses folder.

- The Space Engineers font, space_engineers.ttf, is created by KeenSWH
  and obtained through the Space Engineers ModSDK.

- Json.NET is made by Newtonsoft (James Newton-King)