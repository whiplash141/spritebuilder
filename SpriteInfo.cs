﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Drawing;

namespace SpriteBuilder
{
    public enum SpriteType { TEXTURE, TEXT }

    public enum SpriteAlignment 
    {
        DEFERRED = 0,
        LEFT,
        CENTER,
        RIGHT
    }

    public class SpriteInfo
    {
        public string Name = "";
        public string SpriteType = "";
        public Vector2 Position = default;
        public Vector2 Size = default;
        public SerializableColor Color = new SerializableColor(System.Drawing.Color.White);
        public float Rotation = 0;

        private SpriteType _type = SpriteBuilder.SpriteType.TEXTURE;
        public SpriteType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;

                if (Alignment == SpriteAlignment.DEFERRED)
                {
                    Alignment = _type == SpriteBuilder.SpriteType.TEXT ? SpriteAlignment.LEFT : SpriteAlignment.CENTER;
                }
            }
        }

        public string Font = "Debug";
        public SpriteAlignment Alignment = SpriteAlignment.DEFERRED;


        public SpriteInfo() {}

        public SpriteInfo(string name, string data, SpriteType type)
        {
            Alignment = SpriteAlignment.DEFERRED;
            Name = name;
            SpriteType = data;
            Position = new Vector2(0, 0);
            Size = new Vector2(100, 100);
            Color = new SerializableColor(System.Drawing.Color.White);
            Rotation = type == SpriteBuilder.SpriteType.TEXTURE ? 0 : 1;
            Type = type;
            Font = "Debug";
        }

        public Vector2 GetAlignmentOffset() {

            Vector2 scaledOffset = Constants.TextPositionOffset * -Rotation;
            float magicTextOffset = Constants.MagicTextOffsetPx * Rotation;

            if (Type == SpriteBuilder.SpriteType.TEXT)
            {
                switch (Alignment)
                {
                    default:
                    case SpriteAlignment.LEFT:
                        return scaledOffset;

                    case SpriteAlignment.CENTER:
                        return scaledOffset + 0.5f * new Vector2(-Size.X + magicTextOffset, 0);

                    case SpriteAlignment.RIGHT:
                        return scaledOffset + new Vector2(-Size.X + magicTextOffset, 0);
                }
            }
            else
            {
                switch (Alignment)
                {
                    default:
                    case SpriteAlignment.CENTER:
                        return Vector2.Zero;
                    
                    case SpriteAlignment.LEFT:
                        return new Vector2(0.5f * Size.X, 0);

                    case SpriteAlignment.RIGHT:
                        return new Vector2(-0.5f * Size.X, 0);
                }
            }
        }
        
    }  
}
